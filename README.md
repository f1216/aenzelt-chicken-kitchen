So, here we have console NodeJS application which main function is to manage a restaurant.

It is application for learning purpose so there we have mostly something like skeleton for real application, but not a real one application at all.

Client(Lukash) have some restaurant named "Chicken-kitchen" and he needed some application-manager that helps to manage main
restaurant commands like process what customers want to buy... or process table(like if few people want to buy something), or manage process of ordering some food and ingredients for restaurant, manage restaurant budget, warehouse, operations with trash etc.

Application wrote in object-oriented style.

Here some structure description:

1. We have ck.js which is enter-point of application.
2. Then we have conf.js file, which contain all configurations parameters of application, such a tax %, base path for outputs and etc.
3. Then we have main "app" directory which consist of: some kind of controllers-like classes which control different type of commands that we have(in "managers" directory),
   some models-like classes(in "lib" directory), it is contained methods with something like source logic for every command(business logic).
   And here we have some main classes like restaurant, loader, uploader, it separated classes which have some base restaurant logic and application logic at all.
4. Then we have "input" directory - it is base directory for input command file.
5. Then we have "data" directory - it is base directory for source data storage,
   where we stored customers, base ingredient`s, warehouse data.
6. Then we have "output" directory - it is base directory for output files like output log file, audit file.
7. And of course we have "tests" directory - it is base directory where devs should create their test files.

UPDATE

In a new version of our app we have additional web application based on Express JS server for NodeJs and React frontend.

8. Then we have www.js file - base Express JS server configuration.
9. Then we have "api" directory - it is a base directory for Express JS routes.
10. Then we have "react" directory - it is a base directory for React(here we have "src" directory with all components).
