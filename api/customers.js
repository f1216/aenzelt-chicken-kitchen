let express = require('express');
const fs = require('fs');
const conf = require('../conf');
const parse = require('csv-parse/lib/sync');
let router = express.Router();
const Loader = require('../app/loader');
const Customers = require("../app/lib/customers");

router.get('/getCustomers', function (req, res) {
	const customers = Customers.getCustomers();
	res.json(customers);
});

module.exports = router;
