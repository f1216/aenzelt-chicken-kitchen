let express = require('express');
const fs = require('fs');
const conf = require('../conf');
const parse = require('csv-parse/lib/sync');
const Manager = require('../app/managers/manager');
let router = express.Router();

// Home page route.
router.post('/processInput', function (req, res) {
    try {
        // accessing the file
        const myFile = req.files.file;

        //  mv() method places the file inside public directory
        myFile.mv(conf.input_dir + myFile.name, function (err) {
            if (err) {
                Manager.processInput(
                    req.body.command,
                    req.body.paramsInput,
                    ''
                );
                res.json(true);
            }

            // res.render('index',{isUploaded: true, fileName: fileName, title: 'Express Cheque Processing System '});
            Manager.processInput(
                req.body.command,
                req.body.paramsInput,
                myFile.name
            );
            res.json(true);
        });
    } catch (e) {
        Manager.processInput(req.body.command, req.body.paramsInput, '');
        res.json(true);
    }
});

module.exports = router;
