let express = require('express');
const fs = require('fs');
const conf = require('../../conf');
const parse = require('csv-parse/lib/sync');
let router = express.Router();
const Loader = require('../../app/loader');
const Statistic = require('../../app/lib/statistic');
const ManagerFile = require('../../app/managers/managerFile');

router.get('/getLog', function (req, res) {
    const log = Loader.getLog();
    res.json(log);
});

router.get('/getBaseIngredientsStatistic', function (req, res) {
    res.json(Statistic.getBaseIngredientStatisticFile());
});

module.exports = router;
