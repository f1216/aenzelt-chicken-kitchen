let express = require('express');
const fs = require('fs');
const conf = require('../conf');
const parse = require('csv-parse/lib/sync');
let router = express.Router();

router.get('/getCommands', function (req, res) {
    res.json(Object.keys(conf.allowed_command_list));
});

module.exports = router;
