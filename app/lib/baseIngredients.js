const conf = require('../../conf');
const Food = require('./food');

/** This class is for base ingredients, here we have something like base ingredients model
 * contain not only "save logic" but some method like  for checking is spoiled ingredient or no*/
class BaseIngredients {
    static identifierBaseIngredients = 'Base ingredients';
    static identifierBaseIngredientsPrice = 'Price';
    baseIngredients = [];

    getIdentifierBaseIngredients() {
        return BaseIngredients.identifierBaseIngredients;
    }

    getIdentifierBaseIngredientsPrice() {
        return BaseIngredients.identifierBaseIngredientsPrice;
    }

    load(baseIngredients) {
        this.baseIngredients = baseIngredients;
    }

    getConfSpoilChance() {
        return conf.spoil_chance;
    }

    getBaseIngredients() {
        return this.baseIngredients;
    }

    /** Method for check if is ingredient exist and if it base by his name, we pass name as paramets,
     * such as "Milk", if it exist in base ingredients array we return ingredient object, if no - return false*/
    isBaseIngredient(name) {
        const baseIngredient = this.baseIngredients.filter(
            (item) => item[BaseIngredients.identifierBaseIngredients] === name
        )[0];
        if (baseIngredient === undefined) {
            return false;
        }
        return baseIngredient;
    }

    /** Expect base ingredient object*/
    getName(baseIngredient) {
        return baseIngredient[BaseIngredients.identifierBaseIngredients] ?? '';
    }

    /** Expect base ingredient object*/
    getPrice(baseIngredient) {
        return (
            parseFloat(
                baseIngredient[BaseIngredients.identifierBaseIngredientsPrice]
            ) ?? 0
        );
    }

    /** Expect array of base ingredients objects*/
    getIngredientsPrice(ingredients) {
        let price = 0;
        ingredients.map((item) => {
            price += this.getPrice(item);
        });
        return price;
    }

    /** Expect food or ingredient name, qty, command(just string with name of your command) and then additional classes
     * pass spoiled ingredient to trash and return spoiled ingredient or return false if ingredient not spoiled*/
    isSpoiled(
        ingredient,
        qty = 1,
        Trash,
        Warehouse,
        ManagerFile,
        Audit,
        Tax,
        command,
        Helper
    ) {
        let spoiled = { ingredient: ingredient, qty: 0 };
        for (let i = 0; i < qty; i++) {
            if (Helper.getRandomChance() === this.getConfSpoilChance()) {
                spoiled.qty += 1;
            }
        }

        if (spoiled.qty !== 0) {
            Warehouse.reduceFromQty(Warehouse.getItemByName(ingredient), qty);
            let total = 0;
            let baseIngredient = this.isBaseIngredient(ingredient);
            if (baseIngredient) {
                total = this.getPrice(baseIngredient) * qty;
            } else {
                total =
                    this.getIngredientsPrice(
                        Food.getIngredientsByFood(ingredient, [], BaseIngredients)
                    ) * qty;
            }
            Trash.addTrash(
                ingredient,
                spoiled.qty,
                total,
                ManagerFile,
                Audit,
                Tax,
                command
            );
            return spoiled;
        }

        return false;
    }

    /** Method that gets all ingredients for check, like array with food/ingredients names, and pass it to isSpoiled method
     * return spoiled ingredients array or return false if there no spoiled ingredient*/
    checkSpoiled(
        ingredients,
        Trash,
        Warehouse,
        ManagerFile,
        Audit,
        Tax,
        command,
        Helper
    ) {
        let spoiled = [];

        for (const ingredient of ingredients) {
            let isSpoiled = this.isSpoiled(
                ingredient[BaseIngredients.identifierBaseIngredients],
                1,
                Trash,
                Warehouse,
                ManagerFile,
                Audit,
                Tax,
                command,
                Helper
            );
            if (isSpoiled) spoiled.push(isSpoiled);
        }

        if (spoiled.length !== 0) {
            return spoiled;
        }

        return false;
    }
}

module.exports = new BaseIngredients();
