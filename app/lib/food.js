/** This class is for food, here we have something like food model
 * contain not only "save logic" but some method like for getting base ingredient from full dish*/
class Food {
    static identifierFoodName = 'Food';
    static identifierFoodIngredients = 'Ingredients';
    food = [];

    getIdentifierFoodIngredients() {
        return Food.identifierFoodIngredients;
    }

    getIdentifierFoodName() {
        return Food.identifierFoodName;
    }

    load(food) {
        this.food = food;
    }

    getFood() {
        return this.food;
    }

    /** Method for getting food by name
     * return food object if food exist or false if no*/
    getFoodByName(name) {
        const food = this.getFood().filter(
            (item) => item[Food.identifierFoodName] === name
        );
        if (food.length === 0) {
            return false;
        } else {
            return food;
        }
    }

    /** Method for getting ingredients by food name or food object, it is a recursive method,
     * so if you want to get ingredients from food that have another food like ingredient - you can do it
     * return array of ingredients from food and "subfood" or empty array if there no food or some problems comes up*/
    getIngredientsByFood(food = [], ingredients = [], BaseIngredients) {
        if (!Array.isArray(food)) {
            food = this.getFoodByName(food);
        }
        if (!food) return [];

        food.map((item) => {
            item[Food.identifierFoodIngredients]
                .split(', ')
                .map((ingredient) => {
                    let baseIngredient =
                        BaseIngredients.isBaseIngredient(ingredient);
                    if (!baseIngredient) {
                        this.getIngredientsByFood(
                            ingredient,
                            ingredients,
                            BaseIngredients
                        );
                    } else {
                        ingredients.push(baseIngredient);
                    }
                });
        });

        return ingredients;
    }
}

module.exports = new Food();
