const conf = require("../../conf");
const {command} = require("yargs");

class Statistic {
	popularBaseIngredients = [];
	profitableDishes = [];
	recommendedDishes = [];
	warehouseStates = [];
	commandIterator = {};
	baseIngredientsStatistic = {};

	load (baseIngredientsStatistic) {
		this.baseIngredientsStatistic = baseIngredientsStatistic;
	}

	upload(ManagerFile, BaseIngredients) {
		ManagerFile.write(
			JSON.stringify(this.getBaseIngredientStatistic(BaseIngredients)),
			conf.output_dir + conf.base_ingredient_statistic,
			'w'
		);
	}

	addToPopularBaseIngredients(ingredients, BaseIngredients) {
		for (const ingredient of ingredients) {
			let ingredientItem = this.popularBaseIngredients.find(item => item.ingredient === ingredient[BaseIngredients.getIdentifierBaseIngredients()]);
			if (!ingredientItem) {
				this.popularBaseIngredients.push({
					ingredient: ingredient[BaseIngredients.getIdentifierBaseIngredients()],
					qty: 1
				});
			} else {
				ingredientItem.qty += 1;
			}
		}
	}

	getMostPopularBaseIngredient() {
		if (this.popularBaseIngredients.length > 0) {
			this.popularBaseIngredients.sort((a, b) => (a.qty < b.qty) ? 1 : -1);
			return this.popularBaseIngredients[0].ingredient;
		} else return '';
	}


	addToProfitableDishes(dish, total) {
		let dishItem = this.profitableDishes.find(item => item.dish === dish);
		if (!dishItem) {
			this.profitableDishes.push({
				dish: dish,
				total: total,
			});
		} else {
			dishItem.total += total;
		}
	}

	getProfitableDishes() {
		return this.profitableDishes;
	}

	getMostProfitableDish() {
		if (this.profitableDishes.length > 0) {
			this.profitableDishes.sort((a, b) => (a.total < b.total) ? 1 : -1);
			return this.profitableDishes[0].dish;
		} else return '';
	}

	addToRecommendedDish(dish) {
		let dishItem = this.recommendedDishes.find(item => item.dish === dish);
		if (!dishItem) {
			this.recommendedDishes.push({
				dish: dish,
				qty: 1,
			});
		} else {
			dishItem.qty += 1;
		}
	}

	getMostRecommendedDish() {
		if (this.recommendedDishes.length > 0) {
			this.recommendedDishes.sort((a, b) => (a.qty < b.qty) ? 1 : -1);
			return this.recommendedDishes[0].dish;
		} else return '';
	}

	getCommandIterator(command) {
		if (this.commandIterator[command] !== undefined) {
			return this.commandIterator[command] += 1;
		} else {
			return  this.commandIterator[command] = 1;
		}
	}

	addToWarehouseState(warehouse, command) {
		this.warehouseStates.push(
			{
				command: command + '-' + this.getCommandIterator(command),
				warehouse: warehouse
			}
		);
	}

	getBaseIngredientStatistic(BaseIngredients) {

		const firstState = this.warehouseStates[0].warehouse;
		const lastState = this.warehouseStates[this.warehouseStates.length - 1].warehouse;

		let itemsForSkip = [];
		for(let i = 0; i < firstState.length; i++) {
			//10 4 difference 6     // 10 16 dif -6  // 10 100 6 -x
			let difference = firstState[i].qty - lastState[i].qty;

			if (difference < 0) difference = difference * -1;

			const differencePercent =  (difference * 100) / firstState[i].qty;

			//differencePercent < 20
			if (!BaseIngredients.isBaseIngredient(firstState[i].item)) {
				itemsForSkip.push(firstState[i].item);
			}
		}

		let dataSets = [];
		let labels = [];
		for (const warehouseState of this.warehouseStates) {
			for (const warehouseItem of warehouseState.warehouse) {
				if (itemsForSkip.includes(warehouseItem.item)) {
					continue;
				}
				let exist = false;
				for(const dataSet of dataSets) {
					if (dataSet.label === warehouseItem.item) {
						exist = true;
						dataSet.data.push(warehouseItem.qty);
					}
				}
				if (!exist) {
					dataSets.push({
						data: [warehouseItem.qty],
						label: warehouseItem.item,
						borderColor: '#' + Math.floor(Math.random()*16777215).toString(16),
						fill: false
					})
				}
			}

			labels.push(warehouseState.command);

		}

		return {labels: labels, dataSets: dataSets};
	}

	getBaseIngredientStatisticFile() {
		return this.baseIngredientsStatistic;
	}

}

module.exports = new Statistic;