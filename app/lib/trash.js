const conf = require('../../conf');

/** This class is for trash, here we have something like trash model
 * contain not only "save logic" but some method like for getting trash info*/
class Trash {
    trash = {
        qty: 0,
        pull: [],
    };

    load(trash) {
        this.trash.pull = trash;
    }

    getConfTrashPullFile() {
        return conf.output_dir + conf.trash_pull_filename;
    }

    getConfTrashMax() {
        return conf.tresh_max;
    }

    getTrash() {
        return this.trash;
    }

    getTrashQty() {
        return this.trash.qty;
    }

    getTrashInfo() {
        let info = '';
        if (this.getTrashQty() >= this.getConfTrashMax()) {
            info = 'Restaurant Poisoned';
        } else {
            info = 'Restaurant trash qty - ' + this.getTrashQty();
        }
        return info;
    }

    /** Expect ingredient name*/
    addTrash(ingredient, qty, price, ManagerFile, Audit, Tax, command) {
        qty = parseInt(qty);
        if (isNaN(qty)) return false;
        if (this.getTrashQty() + qty > this.getConfTrashMax()) {
            this.trash.qty = this.getConfTrashMax();
            const log = this.getTrashInfo();
            console.log(log);
            ManagerFile.writeOutput(log);
            return false;
        }
        let trashItem = this.trash.pull.find(
            (item) => item.ingredient === ingredient
        );
        if (trashItem === undefined) {
            this.trash.pull.push({ ingredient: ingredient, qty: 0 });
            trashItem = this.trash.pull.find(
                (item) => item.ingredient === ingredient
            );
        }
        trashItem.qty += qty;

        const tax = price * Tax.getConfTrashTax();
        Tax.addToDailyTrashTax(tax);

        return trashItem;
    }

    /** Throw trash, "clear" object*/
    throw() {
        this.trash = {
            qty: 0,
            pull: [],
        };

        return this.getTrash();
    }
}

module.exports = new Trash();
