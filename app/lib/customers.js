const conf = require('../../conf');

/** This class is for customers, here we have something like customers model
 * contain not only "save logic" but some method like for getting customer budget, reduce budget etc*/
class Customers {
    static identifierCustomerName = 'Regular customer';
    static identifierCustomerAllergies = 'Allergies';
    static identifierCustomerBudget = 'Budget';
    static identifierCustomerOrderCounter = 'Order Counter';

    customers = [];

    load(customers) {
        this.customers = customers;
    }

    upload(ManagerFile) {
        ManagerFile.rewriteData(
            [
                Customers.identifierCustomerName,
                Customers.identifierCustomerAllergies,
                Customers.identifierCustomerBudget,
            ],
            this.customers,
            conf.customers_csv_path
        );
    }

    getCustomers() {
        return this.customers;
    }

    getConfTipsPercent() {
        return conf.tips_percent;
    }

    getConfMaxTipsAmount() {
        return conf.max_tips_amount;
    }

    /** Method for getting customers by name
     * return customer object if customer exist and false if no */
    getCustomerByName(name, logError = true) {
        const customer = this.customers.filter(
            (item) => item[Customers.identifierCustomerName] === name
        )[0];
        if (customer === undefined) {
            if (logError) console.log('Customer ' + name + ' does not exist!');
            return false;
        }
        return customer;
    }

    /** Expect customer object*/
    getBudget(customer) {
        if (customer[Customers.identifierCustomerBudget] === undefined)
            return false;

        return parseFloat(customer[Customers.identifierCustomerBudget]).toFixed(
            2
        );
    }

    /** Method for getting customer allergies,
     * return array of allergens or false if customer does not have
     * expect customer object*/
    getAllergies(customer) {
        if (customer[Customers.identifierCustomerAllergies] === undefined)
            return false;

        return customer[Customers.identifierCustomerAllergies].split(', ');
    }

    /** Expect customer object and number*/
    setBudget(customer, budget) {
        budget = parseFloat(budget.toFixed(2));
        if (
            customer[Customers.identifierCustomerBudget] === undefined ||
            isNaN(budget)
        ) {
            return false;
        }
        return (customer[Customers.identifierCustomerBudget] = budget);
    }

    /** Expect customer object and number and mark is it Tips or no*/
    reduceFromBudget(customer, total, isTips = false) {
        const budget = this.getBudget(customer);
        total = parseFloat(parseFloat(total).toFixed(2));
        if (!budget || isNaN(total)) return false;

        if (isTips && total > budget) {
            total = total - (total - budget);
        }

        return this.setBudget(customer, budget - total);
    }

    /** Method for checking if customer have allergies or no,
     * return array of allergens or false if customer does not have it
     * expect customer object, array of ingredient object and clasess*/
    checkAllergen(customer, ingredients, BaseIngredients) {
        const allergies = this.getAllergies(customer);
        if (!allergies || !Array.isArray(ingredients)) return false;

        for (const ingredient of ingredients) {
            if (allergies.includes(BaseIngredients.getName(ingredient)))
                return BaseIngredients.getName(ingredient);
        }
        return false;
    }

    /** Method for check if customer can afford something or no
     * return true id can or false if no
     * expect customer object, number for check*/
    canAffordIt(customer, total) {
        const budget = this.getBudget(customer);
        if (
            !budget ||
            isNaN(parseFloat(total)) ||
            parseFloat(budget) < parseFloat(total)
        )
            return false;
        return true;
    }

    /** Expect customer object*/
    getOrderCounter(customer) {
        if (customer[Customers.identifierCustomerOrderCounter] !== undefined) {
            return customer[Customers.identifierCustomerOrderCounter];
        } else {
            return 0;
        }
    }

    /** Expect customer object*/
    addOrderCounter(customer) {
        if (customer[Customers.identifierCustomerOrderCounter] === undefined) {
            customer[Customers.identifierCustomerOrderCounter] = 0;
        }
        customer[Customers.identifierCustomerOrderCounter] += 1;
    }

    addOrder(
        customerName = '',
        foodName = '',
        customer,
        food = '',
        total,
        Restaurant,
        ManagerFile
    ) {
        this.addOrderCounter(customer);
        if (this.getOrderCounter(customer) % 3 === 0) {
            total = total - total * Restaurant.getDiscount();
            const log =
                'Customer ' +
                customerName +
                ' get ' +
                Restaurant.getDiscountPercent() +
                '% discount';
            ManagerFile.writeOutput(log);
            console.log(log);
        }
        this.reduceFromBudget(customer, total);
    }

    /** Check if customer will pay tips or no
     * return tips if customer will and false if no*/
    checkTips(total, Helper) {
        if (Helper.getRandomChanceTips() < 0.5) return false;
        let tips = parseFloat((total * this.getConfTipsPercent()).toFixed(2));
        if (this.getConfMaxTipsAmount() === tips) {
            tips = this.getConfMaxTipsAmount();
        }
        if (tips > this.getConfMaxTipsAmount()) {
            tips = tips - (tips - this.getConfMaxTipsAmount());
        }
        return tips;
    }

    //6.11 - Nazar_Kryvous - checking how many wants does customer has


    checkWants(Helper, BaseIngredients, Food, food) {
        let want
        let wantCounter = 0
        let wantIngridientsList = []
        let ingredients = []
        let baseIngridientsList = BaseIngredients.baseIngredients;
        let foodIngridients = Food.getIngredientsByFood(food, ingredients, BaseIngredients)
        let randomWant = Helper.getRandomChanceWants()
        if(randomWant <= 50) want = 0
        else if (randomWant > 50 && randomWant <= 85) want = 1
        else if (randomWant > 85 && randomWant <= 95) want = 2
        else if (randomWant > 95 && randomWant <= 100) want = 3
        for(let i = 0; i <= want-1; i++){
            let rand = baseIngridientsList[Math.floor(Math.random() * baseIngridientsList.length)];
            if(!wantIngridientsList.includes(rand['Base ingredients']))
            wantIngridientsList.push(rand['Base ingredients'])
        }

        for(let element = 0; element < foodIngridients.length; element++){
            const found = wantIngridientsList.includes(foodIngridients[element][ 'Base ingredients'])
            found ? wantCounter += 1 : wantCounter +=0
        }
        // console.log(foodIngridients)
        // console.log(wantIngridientsList)
        console.log(wantCounter + " - Wants")
        return wantCounter
    }







}

module.exports = new Customers();
