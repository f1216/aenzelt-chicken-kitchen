const conf = require('../../conf');
const ManagerFile = require('../managers/managerFile');
const Warehouse = require('./warehouse');
const Restaurant = require('../restaurant');

/** This class is for audit, here we have something like audit model*/
class Audit {
    static identifierResource = 'Resource';
    getIdentifierResource() {
        return Audit.identifierResource;
    }

    audit = {
        Resource: [],
    };

    auditLength = 0;

    load() {
        this.addItem(
            false,
            'INIT',
            Warehouse.getWarehouse(),
            Restaurant.getBudgetInfo(),
            Audit.identifierResource
        );
        this.addItem(false, 'START', [], '', Audit.identifierResource);
    }

    getAuditResource() {
        return this.audit.Resource;
    }

    setAuditLength(length) {
        this.auditLength = length;
    }

    getAuditLength() {
        return this.auditLength;
    }

    addItem(
        command,
        info,
        warehouse,
        budgetInfo,
        type = Audit.identifierResource
    ) {
        let item = '';
        if (command) item = 'command: ' + command + ',';
        warehouse.length !== 0
            ? (warehouse = 'Warehouse: ' + JSON.stringify(warehouse))
            : (warehouse = '');

        item += info + '\r\n' + warehouse + '\r\n' + budgetInfo;
        this.audit[type].push(item.trim('\r\n'));
        return item;
    }

    addItemLog(log,  type = Audit.identifierResource) {
        this.audit[type].push(log + '\r\n');
    }

    upload(type, Tax) {
        this.setAuditLength(this.getAuditResource().length);
        this.addItem(
            false,
            Tax.getDailyTaxInfo(Restaurant) +
                '\r\n' +
                Tax.getDailyTipsTaxInfo(),
            [],
            '',
            Audit.identifierResource
        );
        this.addItem(false, 'END', [], '', Audit.identifierResource);
        const audit = this.audit[type].join('\r\n');
        ManagerFile.write(audit, conf.output_dir + conf.audit_filename);
    }
}

module.exports = new Audit();
