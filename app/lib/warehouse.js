const BaseIngredient = require('./baseIngredients');
const Food = require('./food');
const conf = require('../../conf');

/** This class is for warehouse, here we have something like warehouse model
 * contain not only "save logic" but some method like for check availability of some ingredient on warehouse*/
class Warehouse {
    static identifierWarehouseItem = 'item';
    static identifierWarehouseQty = 'qty';
    warehouse = [];
    warehouseTemp = [];

    load(warehouse) {
        this.warehouse = warehouse;
    }

    upload(ManagerFile) {
        ManagerFile.rewriteData(
            [
                Warehouse.identifierWarehouseItem,
                Warehouse.identifierWarehouseQty,
            ],
            this.getWarehouse(),
            conf.warehouse_csv_path
        );
    }

    getWarehouse() {
        return this.warehouse;
    }

    getConfTotalMaxQty() {
        return conf.warehouse.total_max;
    }

    getConfIngredientsMaxQty() {
        return conf.warehouse.total_ingredients_max;
    }

    getConfDishesMaxQty() {
        return conf.warehouse.total_dishes_max;
    }

    getCurrentQty() {
        let qty = 0;
        this.getWarehouse().map((item) => {
            qty += this.getQty(item);
        });
        return qty;
    }

    getConfDishesWithAllergies() {
        return conf.warehouse.dishes_with_allergies;
    }

    getConfCommandsWithReduce() {
        return conf.warehouse.commands_with_reduce;
    }

    getConfAllergiesPercent() {
        return conf.allergies_percent;
    }

    /** Method for creating copy of current warehouse state,
     * this copy we use every time when we check availability of some food
     * or ingredient before we start to process some command (Buy, Table)*/
    createCopy() {
        this.warehouseTemp = JSON.parse(JSON.stringify(this.warehouse));
    }

    addItem(name, qty = 0) {
        const item = { item: name, qty: qty };
        this.warehouse.push(item);
        return item;
    }

    /** Method for getting warehouse item by name
     * return warehouse item object if it exist or false if no*/
    getItemByName(name) {
        const warehouse = this.warehouse.filter(
            (item) => item[Warehouse.identifierWarehouseItem] === name
        )[0];
        if (warehouse === undefined) {
            return this.addItem(name);
        }
        return warehouse;
    }

    getTempItemByName(name) {
        const warehouse = this.warehouseTemp.filter(
            (item) => item[Warehouse.identifierWarehouseItem] === name
        )[0];
        if (warehouse === undefined) {
            return this.addItem(name);
        }
        return warehouse;
    }

    /** Expect warehouse object*/
    getQty(item, isBase = false) {
        if (
            isBase &&
            (item[Warehouse.identifierWarehouseItem] === undefined ||
                item[Warehouse.identifierWarehouseQty] === 0)
        ) {
            item[Warehouse.identifierWarehouseQty] = conf.default_item_qty;
        }
        if (item[Warehouse.identifierWarehouseItem] === undefined) return 0;
        return parseInt(item[Warehouse.identifierWarehouseQty]);
    }

    /** Expect warehouse object*/
    setQty(item, qty) {
        if (
            item[Warehouse.identifierWarehouseQty] === undefined ||
            !Number.isInteger(parseInt(qty))
        ) {
            return false;
        }
        return (item[Warehouse.identifierWarehouseQty] = qty);
    }

    /** Expect warehouse object and qty*/
    reduceFromQty(item, qty) {
        let itemQty = this.getQty(item);
        itemQty -= qty;
        if (itemQty < 0) {
            itemQty = 0;
        }
        this.setQty(item, itemQty);
    }

    /** Expect warehouse object and qty*/
    addQty(item, qty) {
        const itemQty = this.getQty(item);
        this.setQty(item, itemQty + qty);
    }

    /** Method for checking how much qty we can order when process Order command
     * return true if we can order some ingredient or false if we have some problems
     * belong to the new requirements we need to order only qty that we can do and trash remainder */
    checkWarehouseQtyConf(
        ingredient,
        qty,
        item,
        BaseIngredients,
        Food,
        Warehouse,
        Trash,
        Restaurant,
        ManagerFile,
        Audit,
        Tax,
        command
    ) {
        let returnData = true;

        const baseIngredient = BaseIngredients.isBaseIngredient(ingredient);

        const currentWarehouseQty = this.getCurrentQty();
        const maxAllowedQty = this.getConfTotalMaxQty();

        const currentWarehouseItemQty = this.getQty(item);
        const canAddIngredientsQty =
            this.getConfIngredientsMaxQty() - currentWarehouseItemQty;
        const canAddDishesQty =
            this.getConfDishesMaxQty() - currentWarehouseItemQty;
        let limit = 0;
        let canAddQty = 0;
        if (baseIngredient) {
            limit = this.getConfIngredientsMaxQty();
            canAddQty = canAddIngredientsQty;
        } else {
            limit = this.getConfDishesMaxQty();
            canAddQty = canAddDishesQty;
        }

        const wantToAddItemQty = qty;

        let canAddItemQtyREAL = 0;

        if (
            currentWarehouseQty + wantToAddItemQty > maxAllowedQty &&
            currentWarehouseQty + canAddQty < maxAllowedQty
        ) {
            canAddItemQtyREAL = canAddQty;
            returnData = ' total limit - ' + this.getConfTotalMaxQty();
        } else if (
            currentWarehouseQty + wantToAddItemQty > maxAllowedQty &&
            currentWarehouseQty + canAddQty > maxAllowedQty
        ) {
            canAddItemQtyREAL =
                currentWarehouseQty + canAddQty - maxAllowedQty + 1;
            returnData = ' total limit - ' + this.getConfTotalMaxQty();
        } else if (wantToAddItemQty > canAddQty) {
            canAddItemQtyREAL = canAddQty;
            returnData = ' limit - ' + limit;
        } else {
            canAddItemQtyREAL = wantToAddItemQty;
        }

        const wasted = wantToAddItemQty - canAddItemQtyREAL;

        if (returnData !== true) {
            const log =
                'Ordered: ' +
                canAddItemQtyREAL +
                ', wasted ' +
                wasted +
                ', ' +
                returnData;

            console.log(log);
            ManagerFile.writeOutput(log);
            Audit.addItem(
                command,
                log,
                this.getWarehouse(),
                Restaurant.getBudgetInfo()
            );

            this.addQty(item, canAddItemQtyREAL);
            let total = 0;
            let baseIngredient = BaseIngredients.isBaseIngredient(ingredient);
            let space = 1;
            if (baseIngredient) {
                total = BaseIngredients.getPrice(baseIngredient) * qty;
            } else {
                let ingredients = Food.getIngredientsByFood(Food.getFoodByName(ingredient), [], BaseIngredients);
                space = ingredients.length;
                total =
                    BaseIngredients.getIngredientsPrice(
                        ingredients
                    ) * qty;
            }
            Trash.addTrash(
                ingredient,
                wasted * space,
                total,
                ManagerFile,
                Audit,
                Tax,
                command
            );
            Restaurant.reduceFromBudget(
                total,
                Warehouse,
                ManagerFile,
                Audit,
                command
            );
            return false;
        }
        return true;
    }

    /** Method for checking availability of some ingredient at warehouse, it`s a recursive method
     * expect food name or food object as a parameter
     * use copy of current warehouse state to check it
     * return true if all ingredients available and false if no*/
    checkAvailability(
        food = [],
        qty = 1,
        Food,
        BaseIngredients,
        createCopy = false
    ) {
        if (createCopy) {
            this.createCopy();
        }
        let foodData = false;
        if (!Array.isArray(food)) {
            foodData = Food.getFoodByName(food);
        }
        if (!foodData && !Array.isArray(food)) {
            let baseIngredient = BaseIngredients.isBaseIngredient(food);
            if (!baseIngredient) {
                return false;
            }
            if (
                baseIngredient &&
                this.getQty(this.getTempItemByName(food), false) < qty
            ) {
                return false;
            }
            this.reduceFromQty(this.getTempItemByName(food), qty);
            return true;
        }
        if (Array.isArray(food)) {
            foodData = food;
        }

        let returnData = true;
        foodData.map((item) => {
            let baseIngredient = BaseIngredients.isBaseIngredient(
                item[Food.getIdentifierFoodName()]
            );
            let warehouseItem = this.getTempItemByName(
                item[Food.getIdentifierFoodName()]
            );
            if (!baseIngredient && this.getQty(warehouseItem, false) < qty) {
                item[Food.getIdentifierFoodIngredients()]
                    .split(', ')
                    .map((ingredient) => {
                        if (!returnData) {
                            return;
                        }
                        returnData = this.checkAvailability(
                            ingredient,
                            1,
                            Food,
                            BaseIngredients
                        );
                    });
                return returnData;
            }
            if (baseIngredient && this.getQty(warehouseItem, false) < qty) {
                returnData = false;
                return false;
            }
            this.reduceFromQty(warehouseItem, qty);
            // if (!baseIngredient && this.getQty(item[Food.getIdentifierFoodName()], false) > qty)
        });
        return returnData;
    }

    /** Method for reduce food or food ingredients from warehouse
     * we use that method when we want to check if customer have allergies belong to requirements(delete if client have allergies)
     * return false if something went wrong*/
    //не знімати страву, якщо готуєш з її інгредієнтів(не йти в мінус)
    processFood(
        food = [],
        qty = 1,
        Food,
        BaseIngredients,
        allergies = false,
        Warehouse,
        Trash,
        Restaurant,
        ManagerFile,
        Audit,
        Tax,
        command
    ) {
        let foodData = false;
        if (!Array.isArray(food)) {
            foodData = Food.getFoodByName(food);
        }
        if (!foodData && !Array.isArray(food)) {
            let baseIngredient = BaseIngredients.isBaseIngredient(food);
            let warehouseItem = this.getItemByName(food);
            if (baseIngredient && this.getQty(warehouseItem, false) < qty) {
                return false;
            }
            this.reduceFromQty(warehouseItem, qty);
            return;
        }
        if (Array.isArray(food)) {
            foodData = food;
        }

        return foodData.map((item) => {
            let baseIngredient = BaseIngredients.isBaseIngredient(
                item[Food.getIdentifierFoodName()]
            );
            let warehouseItem = this.getItemByName(
                item[Food.getIdentifierFoodName()]
            );
            if (!baseIngredient && this.getQty(warehouseItem, false) < qty) {
                item[Food.getIdentifierFoodIngredients()]
                    .split(', ')
                    .map((ingredient) => {
                        this.processFood(
                            ingredient,
                            1,
                            Food,
                            BaseIngredients,
                            true,
                            Warehouse,
                            Trash,
                            Restaurant,
                            ManagerFile,
                            Audit,
                            Tax,
                            command
                        );
                    });
                //6.2.5 keep dish but reduce ingredients price + 25% from restaurant budget
                if (
                    allergies &&
                    this.checkWarehouseQtyConf(
                        item[Food.getIdentifierFoodName()],
                        qty,
                        warehouseItem,
                        BaseIngredients,
                        Food,
                        Warehouse,
                        Trash,
                        Restaurant,
                        ManagerFile,
                        Audit,
                        Tax,
                        command
                    )
                ) {
                    this.addQty(warehouseItem, 1);
                    const price = BaseIngredients.getIngredientsPrice(
                        Food.getIngredientsByFood(food, [], BaseIngredients)
                    );
                    Restaurant.reduceFromBudget(
                        price + price * this.getConfAllergiesPercent(),
                        Warehouse,
                        ManagerFile,
                        Audit,
                        command
                    );
                }
                return;
            }
            if (
                !baseIngredient &&
                this.getQty(warehouseItem, false) > qty &&
                !allergies
            ) {
                //не забирв бюджет в разі,якщо страва була на складі і просто не підійшла(алергія кастомера)
                this.reduceFromQty(warehouseItem, qty);
            }
            if (baseIngredient && this.getQty(warehouseItem, false) < qty) {
                return false;
            }

            this.reduceFromQty(warehouseItem, qty);
        });
    }
}

module.exports = new Warehouse();
