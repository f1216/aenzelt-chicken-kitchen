class Time {

	currentTimeState = 0;

	getCurrentTimeState() {
		return this.currentTimeState;
	}

	setCurrentTimeState(time) {
		this.currentTimeState = time;
	}

	addToCurrentTimeState(time) {
		this.currentTimeState += time;
	}


	getTimeForCommand(
		command = '',
		data,
		Food,
		BaseIngredients,
		Helper,
		conf,
		Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse, Trash, ManagerFile, Tax) {

		let time = 0;
		switch (command) {
			case 'Buy': time = this.getTimeForBuy(command, data, Food, BaseIngredients, Helper, conf, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse); break;
			case 'Table': time = this.getTimeForTable(command, data, Food, BaseIngredients, Helper, conf, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse); break;
			case 'Order': time = this.getTimeForOrder(data, BaseIngredients, conf); break;
			case 'Delay': time = this.getTimeForDelay(data); break;
			case 'Budget': time = this.getTimeForBudget(); break;
			case 'Audit': time = this.getTimeForAudit(); break;
			default: time = 0; break;
		}

		let newTimeState = this.getCurrentTimeState() + time;
		if (newTimeState > 100) {
			this.setCurrentTimeState(newTimeState % 100);
			let numberForSpoilingCheck = Math.floor(newTimeState / 100);
			this.timeSpoiling(
				numberForSpoilingCheck,
				Trash,
				Warehouse,
				ManagerFile,
				Audit,
				Tax,
				command,
				Helper,
				BaseIngredients, Food)
		} else {
			this.addToCurrentTimeState(newTimeState);
		}

		return time;
	}

	timeSpoiling(numberForSpoilingCheck,
	             Trash,
	             Warehouse,
	             ManagerFile,
	             Audit,
	             Tax,
	             command,
	             Helper,
	             BaseIngredients, Food) {
		for (let i = 0; i < numberForSpoilingCheck; i++) {
			for(const warehouseItem of Warehouse.getWarehouse()) {
				let ingredients = [];
				let ingredient = BaseIngredients.isBaseIngredient(warehouseItem.item);
				if (ingredient) {
					ingredients.push(ingredient);
				} else {
					ingredients = Food.getIngredientsByFood(Food.getFoodByName(warehouseItem.item), [], BaseIngredients);
				}

				BaseIngredients.checkSpoiled(
					ingredients,
					Trash,
					Warehouse,
					ManagerFile,
					Audit,
					Tax,
					command,
					Helper
				);

			}
		}
	}

	getTimeForDelay(data) {
		return data[0];
	}

	getTimeForBuy(command, data, Food, BaseIngredients, Helper, conf, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse) {
		data = this.parseCommandBuyData(data, Recommendations, Customers, BaseIngredients, Restaurant, Audit, Food, Statistic, Warehouse);
		let strForCount = this.prepareStringForCount(command, data.customers, data.dishes, Food, BaseIngredients);
		let numberOfVowels = this.getNumberOfVowels(strForCount);
		return Math.ceil(this.calculation(numberOfVowels, Helper) + conf.base_time_for_buy);
	}

	getTimeForTable(command, data, Food, BaseIngredients, Helper, conf, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse) {
		data = this.parseCommandTableData(data, Recommendations, Customers, BaseIngredients, Restaurant, Audit, Food, Statistic, Warehouse);
		let strForCount = this.prepareStringForCount(command, data.customers, data.dishes, Food, BaseIngredients);
		let numberOfVowels = this.getNumberOfVowels(strForCount);
		return Math.ceil(this.calculation(numberOfVowels, Helper) * (Math.pow(conf.base_time_for_table_person, data.customers.length)));
	}

	getTimeForOrder(data, BaseIngredients, conf) {
		let time = 1;
		let ordersArray = this.parseCommandOrderData(data);
		for (const data of ordersArray) {
			const ingredient = data[0];
			if (!BaseIngredients.isBaseIngredient(ingredient)) {
				time = time * conf.base_time_for_order_dish;
			} else {
				time = time * conf.base_time_for_order_ingredient;
			}
		}
		return ((conf.base_time_for_order_total * ordersArray.length) * time) + conf.base_time_for_order;
	}


	parseCommandBuyData(data, Recommendations, Customers, BaseIngredients, Restaurant, Audit, Food, Statistic, Warehouse) {
		//First of all we prepare data(customer, food, food ingredients, total cost)
		const customerName = data[0];
		let foodName = data[1];
		if (foodName === 'Recommend') {
			let ingredients = [];
			ingredients.push(data[2]);
			foodName = Recommendations.getFoodWhichUserCanBuy(ingredients, Customers.getBudget(Customers.getCustomerByName(customerName)), BaseIngredients, Restaurant, Customers.getAllergies(Customers.getCustomerByName(customerName)), customerName, Audit, Food, Statistic, Warehouse, true);
			if (foodName === '') {
				return false;
			}
		}

		return {
			customers: [customerName],
			dishes: [foodName]
		}
	}

	parseCommandTableData(data, Recommendations, Customers, BaseIngredients, Restaurant, Audit, Food, Statistic, Warehouse) {
		let pooled = false;
		if (data[0] === 'Pooled') {
			pooled = true;
			data.shift();
		}

		let customers = [];
		let dishes = [];

		// Here we create arrays with customers and dishes that we need to process table
		for (const item of data) {
			if (item.search(/Recommend/) !== -1) {
				dishes.push(item);
			} else {
				let itemArray = item.split(', ');
				for(let itemData of itemArray) {
					if (Customers.getCustomerByName(itemData, false)) {
						customers.push(itemData);
					} else if (itemData.search(/Recommend/) !== -1) {
						let ingredients = itemData.split(', ');
						ingredients.shift();
						let foodName = Recommendations.getFoodWhichUserCanBuy(ingredients, 10000000, BaseIngredients, Restaurant, false, '', Audit, Food, Statistic, Warehouse, true);
						dishes.push(foodName);
					} else {
						dishes.push(itemData);
					}
				}
			}

		}

		return {
			customers: customers,
			dishes: dishes
		}
	}

	parseCommandOrderData(data) {
		// create array with orders ingredient,
		// that algorithm work properly both if we want to order one ingredient or two and more
		let perChunk = 2; // items per chunk

		let ordersArray = data.reduce((resultArray, item, index) => {
			const chunkIndex = Math.floor(index / perChunk);
			if (!resultArray[chunkIndex]) {
				resultArray[chunkIndex] = []; // start a new chunk
			}
			resultArray[chunkIndex].push(item);
			return resultArray;
		}, []);
		//

		return ordersArray;
	}

	getTimeForBudget() {
		return 250;
	}

	getTimeForAudit() {
		return 100;
	}

	prepareStringForCount(
		command = '',
		customers = [],
		dishes = [],
		Food = [],
		BaseIngredients) {
		let strForCount = command;

		for(let customer of customers) {
			strForCount += customer;
		}

		for (let dish of dishes) {
			let ingredients = Food.getIngredientsByFood(dish, [], BaseIngredients);
			if (ingredients.length === 0) {
				strForCount += dish;
			} else {
				for(let ingredient of ingredients) {
					strForCount += ingredient[BaseIngredients.getIdentifierBaseIngredients()];
				}
			}
		}

		return strForCount;
	}

	getNumberOfVowels(strForCount) {
		return (strForCount.match(/[aeiou]/gi) || []).length;
	}

	calculation(numberOfVowels, Helper) {
		let additional =  Math.ceil((numberOfVowels * 20) / 100);
		if (Helper.getRandomChanceTime() < 0.5) {
			numberOfVowels -= additional;
		} else {
			numberOfVowels +=additional;
		}
		return numberOfVowels;
	}

}

module.exports = new Time;