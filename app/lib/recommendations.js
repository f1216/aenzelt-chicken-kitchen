class Recommendations {
	getFoodWhichUserCanBuy(ingredient, budget, BaseIngredients, Restaurant, allergies, name, Audit, Food, Statistic, Warehouse, timeCheck = false) {

		let recommendDishes = new Set();
		const allDishes = Food.getFood();
		for (let i = 0; i < allDishes.length; i++) {
			let result = Food.getIngredientsByFood(allDishes[i][Food.getIdentifierFoodName()], [], BaseIngredients)
			let numberOfCoincidences = new Set();
			for (let y = 0; y < result.length; y++) {
				for (let t = 0; t < ingredient.length; t++) {
					if (result[y]["Base ingredients"] === ingredient[t]) {
						numberOfCoincidences.add(ingredient[t])
					}
				}
				if (numberOfCoincidences.size === ingredient.length) {
					recommendDishes.add((allDishes[i]))
				}
			}
		}

		let array = Array.from(recommendDishes)
		let foodNameWhichUserCanBuy = ""
		let foodPriceWhichUserCanBuy = 0
		for (let j = 0; j < array.length; j++) {
			let ingredients = Food.getIngredientsByFood(array[j][Food.getIdentifierFoodName()], [], BaseIngredients)
			let total = BaseIngredients.getIngredientsPrice(ingredients);
			if (budget >= total) {
				for (let y = 0; y < ingredients.length; y++) {
					if (ingredients[y][BaseIngredients.getIdentifierBaseIngredients()] === allergies) {
						foodNameWhichUserCanBuy = ""
						foodPriceWhichUserCanBuy = 0
						break
					} else {
						if (total > foodPriceWhichUserCanBuy) {
							foodNameWhichUserCanBuy = array[j];
							foodPriceWhichUserCanBuy = total;
						}
					}
				}
			}
		}
		if (!timeCheck) {
			Audit.addItem(
				"Recommended food",
				`Recommended food: ${array}. User selection: ${foodNameWhichUserCanBuy[Food.getIdentifierFoodName()]}.`,
				Warehouse.getWarehouse(),
				Restaurant.getBudgetInfo()
			);
		}


		Statistic.addToRecommendedDish(foodNameWhichUserCanBuy[Food.getIdentifierFoodName()]);
		return foodNameWhichUserCanBuy[Food.getIdentifierFoodName()];
	}
}

module.exports = new Recommendations;