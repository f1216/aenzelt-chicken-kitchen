const Customers = require('./lib/customers');
const Food = require('./lib/food');
const BaseIngredients = require('./lib/baseIngredients');
const Warehouse = require('./lib/warehouse');
const Trash = require('./lib/trash');
const Audit = require('./lib/audit');
const Restaurant = require('./restaurant');
const ManagerFile = require('./managers/managerFile');
const Statistic = require('./lib/statistic');
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const conf = require('../conf');

/** This class have functionality that response only for getting data and pass this data to classes*/
class Loader {
    /** Here we call loaders of different classes and pass data from files to it*/
    load() {
        Customers.load(this.getCustomers());
        Food.load(this.getFood());
        BaseIngredients.load(this.getBaseIngredients());
        Warehouse.load(this.getWarehouse());
        Trash.load(this.getTrash());
        Restaurant.load();
        Audit.load();
        ManagerFile.load(Restaurant);
        Statistic.load(this.getBaseIngredientsStatistic())
    }

    getCustomers() {
        try {
            const csvString = fs.readFileSync(conf.customers_csv_path, {
                encoding: 'utf8',
                flag: 'r',
            });
            return parse(csvString, { columns: true });
        } catch (e) {
            return [];
        }
    }

    getFood() {
        try {
            const csvString = fs.readFileSync(conf.food_csv_path, {
                encoding: 'utf8',
                flag: 'r',
            });
            return parse(csvString, { columns: true });
        } catch (e) {
            return [];
        }
    }

    getBaseIngredients() {
        try {
            const csvString = fs.readFileSync(conf.base_ingredients_csv_path, {
                encoding: 'utf8',
                flag: 'r',
            });
            return parse(csvString, { columns: true });
        } catch (e) {
            return [];
        }
    }

    getWarehouse() {
        try {
            const csvString = fs.readFileSync(conf.warehouse_csv_path, {
                encoding: 'utf8',
                flag: 'r',
            });
            return parse(csvString, { columns: true });
        } catch (e) {
            return [];
        }
    }

    getTrash() {
        try {
            const csvString = fs.readFileSync(
                conf.output_dir + conf.trash_pull_filename,
                { encoding: 'utf8', flag: 'r' }
            );
            return parse(csvString, { columns: true });
        } catch (e) {
            return [];
        }
    }

    getLog() {
        let log = []; //{id: 1, log: ''}
        try {
            const csvString = fs.readFileSync(
                conf.output_dir + conf.log_filename,
                {
                    encoding: 'utf8',
                    flag: 'r',
                }
            );

            let i = 1;
            csvString.split('\r\n').map((item) => {
                log.push({ id: i, log: item });
                i++;
            });
        } catch (e) {}
        return log;
    }

    getBaseIngredientsStatistic() {
        try {
            const csvString = fs.readFileSync(conf.output_dir + conf.base_ingredient_statistic, {
                encoding: 'utf8',
                flag: 'r',
            });
            return JSON.parse(csvString);
        } catch (e) {
            return [];
        }
    }
}

module.exports = new Loader();
