const Customers = require('../lib/customers');
const Food = require('../lib/food');
const BaseIngredients = require('../lib/baseIngredients');
const ManagerFile = require('./managerFile');
const Restaurant = require('../restaurant');
const Warehouse = require('../lib/warehouse');
const Trash = require('../lib/trash');
const ManagerBudget = require('./managerBudget');
const ManagerBuy = require('./managerBuy');
const ManagerOrder = require('./managerOrder');
const ManagerTable = require('./managerTable');
const ManagerConditions = require('./managerConditions');
const Audit = require('../lib/audit');
const ManagerTrash = require('./managerTrash');
const conf = require('../../conf');
const Tax = require('../tax');
const Helper = require('../helper');
const Recommendations = require('../lib/recommendations');
const Statistic = require('../lib/statistic');
const Time = require('../lib/time');

/** This class is main manager(controller) of our application,
 * this class controls relations between command and specialized command managers*/
class Manager {
    /** Here we call file manager and get all commands from input file and push it into array
     * (it will be good if logic of this file manager method will be here, small refactoring)*/
    prepareCommandList(command = '', data = [], fileName = '', ManagerFile, Time, Food, BaseIngredients, Helper, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse) {
        !command ? (command = '') : command;
        !data ? (data = []) : data;
        !fileName ? (fileName = '') : fileName;

        if (!Array.isArray(data)) {
            data = data.split(',');
        }

        let commandList = [];
        commandList.push({command: command, data: data});
        commandList = commandList.concat(
            ManagerFile.getCommandListFromInput(fileName)
        );

        for(const commandListItem of commandList) {
            commandListItem.time = Time.getTimeForCommand(commandListItem.command,
                commandListItem.data,
                Food,
                BaseIngredients,
                Helper,
                conf,
                Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse,
                Trash, ManagerFile, Tax);
        }
        commandList.sort((a, b) => (a.time > b.time) ? 1 : -1);
        return commandList;
    }

    /** Here we have main method of this class,
     * we perform preparing array of commands here
     * and call process command item method for every single command*/
    processInput(command = '', data = '', fileName = '') {

        const commandList = this.prepareCommandList(
            command,
            data,
            fileName,
            ManagerFile,
            Time,
            Food,
            BaseIngredients,
            Helper,
            Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse
        );

        console.log(Restaurant.getBudgetInfo());
        for (const commandItem of commandList) {
            this.processCommandItem(commandItem);
        }
        console.log(Restaurant.getBudgetInfo());
        console.log(Restaurant.getDailyProfitInfo(Tax));
        console.log(
            Tax.getDailyTaxInfo(Restaurant) +
            '\r\n' +
            Tax.getDailyTipsTaxInfo() +
            '\r\n' +
            Tax.getDailyTrashTaxInfo()
        );

        const log = 'Most popular ingredient: ' + Statistic.getMostPopularBaseIngredient() + '\n' +
            'Most profitable dish: ' + Statistic.getMostProfitableDish() + '\n' +
            'Total dishes: ' + Statistic.getProfitableDishes().length + '\n' +
            'Most recommended dish: ' + Statistic.getMostRecommendedDish() + '\n';
        ManagerFile.writeOutput(log);
    }

    /** Here we have command that decide which manager need to be called
     * we have switcher here and a few checkers for budget/trash states and checker for allowed commands*/
    processCommandItem(commandItem) {
        if (commandItem.command === '') return false;
        if (
            conf.allowed_command_list[commandItem.command] === undefined ||
            conf.allowed_command_list[commandItem.command] === false
        ) {
            const log = 'Command ' + commandItem.command + ' not allowed';
            ManagerFile.writeOutput(log);
            console.log(log);
            return false;
        }
        if (
            Restaurant.getBudget() < 0 &&
            commandItem.command !== 'Budget' &&
            commandItem.command !== 'Audit'
        ) {
            const log = Restaurant.getBudgetInfo();
            console.log(log);
            ManagerFile.writeOutput(log);
            return false;
        }
        if (
            Trash.getTrashQty() >= Trash.getConfTrashMax() &&
            commandItem.command !== 'Audit'
        ) {
            const log = Trash.getTrashInfo();
            console.log(log);
            ManagerFile.writeOutput(log);
            return false;
        }

        switch (commandItem.command) {
            case 'Buy':
                ManagerBuy.processBuy(
                    commandItem.data,
                    Food,
                    BaseIngredients,
                    Warehouse,
                    Trash,
                    Customers,
                    Restaurant,
                    Manager,
                    ManagerConditions,
                    ManagerFile,
                    Audit,
                    Tax,
                    Helper,
                    Recommendations,
                    Statistic
                );
                break;
            case 'Table':
                ManagerTable.processTable(
                    commandItem.data,
                    Food,
                    BaseIngredients,
                    Warehouse,
                    Trash,
                    Customers,
                    Restaurant,
                    Manager,
                    ManagerConditions,
                    ManagerFile,
                    ManagerBuy,
                    Audit,
                    Tax,
                    Helper,
                    Recommendations,
                    Statistic
                );
                break;
            case 'Budget':
                ManagerBudget.processBudget(
                    commandItem.data,
                    Warehouse,
                    Restaurant,
                    ManagerFile,
                    Audit,
                    Tax
                );
                break;
            case 'Order':
                ManagerOrder.processOrder(
                    commandItem.data,
                    BaseIngredients,
                    Food,
                    Warehouse,
                    Trash,
                    Restaurant,
                    ManagerFile,
                    Audit,
                    Tax,
                    Helper
                );
                break;
            case 'Audit':
                Audit.upload(Audit.getIdentifierResource(), Tax);
                break;
            case 'Throw trash away':
                ManagerTrash.throwTrash(Trash, ManagerFile);
                break;
        }
        let timeLog = commandItem.command + ' Command takes: ' + commandItem.time + ' time';
        Audit.addItemLog(timeLog);
        ManagerFile.writeOutput(timeLog);
        console.log(timeLog);
        Statistic.addToWarehouseState(Warehouse.getWarehouse(), commandItem.command);
    }
}

module.exports = new Manager();
