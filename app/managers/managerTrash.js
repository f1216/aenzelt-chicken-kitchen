/** This class is trash manager, here we process trash command
 * for now we have only one command to throw trash*/
class ManagerTrash {
    throwTrash(Trash, ManagerFile) {
        const trash = Trash.getTrash();
        ManagerFile.rewriteData(
            ['pull'],
            trash.pull,
            Trash.getConfTrashPullFile()
        );
        return Trash.throw();
    }
}

module.exports = new ManagerTrash();
