/** This class is buy command manager that perform all operation related to buying process
 * (like check different condition: customer/restaurant budget, spoiled ingredient or no, warehouse state)*/
class ManagerBuy {
    processBuy(
        data,
        Food,
        BaseIngredients,
        Warehouse,
        Trash,
        Customers,
        Restaurant,
        Manager,
        ManagerConditions,
        ManagerFile,
        Audit,
        Tax,
        Helper,
        Recommendations,
        Statistic
    ) {

        if (!Array.isArray(data) || !data) return false;

        //First of all we prepare data(customer, food, food ingredients, total cost)
        const customerName = data[0];
        let foodName = data[1];
        if (foodName === 'Recommend') {
            let ingredients = [];
            ingredients.push(data[2]);
            foodName = Recommendations.getFoodWhichUserCanBuy(ingredients, Customers.getBudget(Customers.getCustomerByName(customerName)), BaseIngredients, Restaurant, Customers.getAllergies(Customers.getCustomerByName(customerName)), customerName, Audit, Food, Statistic, Warehouse);
            if (foodName === '') {
                return false;
            }
        }

        const customer = Customers.getCustomerByName(customerName);
        const food = Food.getFoodByName(foodName);
        if (!customer || !food) return false;
        const ingredients = Food.getIngredientsByFood(
            food,
            [],
            BaseIngredients
        );

        Statistic.addToPopularBaseIngredients(ingredients, BaseIngredients);

        // Nazar_Kryvous 6.11.1. I really want to eat something
        const wants = Customers.checkWants(Helper, BaseIngredients, Food, food);


        const total = this.calculateBuyTotal(
            ingredients,
            BaseIngredients,
            Restaurant
        );

        //Then we check conditions in another one manager created especially for those needs
        //if there no problems - we can process next operations
        switch (
            ManagerConditions.conditionsChecks(
                data,
                customerName,
                foodName,
                customer,
                food,
                ingredients,
                total,
                Food,
                BaseIngredients,
                Warehouse,
                Trash,
                Customers,
                Restaurant,
                Manager,
                ManagerFile,
                Audit,
                Tax,
                'process buy',
                false,
                Helper
            )
        ) {
            case 'ERROR_ALLERGEN':
                return false;
            case 'ERROR_LOW_BUDGET':
                return false;
            case 'ERROR_WAREHOUSE':
                return false;
            case 'ERROR_SPOILED':
                return false;
            case 'ERROR_NO_INGREDIENTS':
                return false;
            case 'ALL_RIGHT':
                // if there no problems we can perform buy action and place buy
                this.placeBuy(
                    customerName,
                    foodName,
                    customer,
                    food,
                    total,
                    Food,
                    BaseIngredients,
                    Warehouse,
                    Trash,
                    Customers,
                    Restaurant,
                    ManagerFile,
                    Audit,
                    Tax,
                    'Buy',
                    Helper,
                    wants,
                    Statistic
                );
                break;
        }

        return true;
    }

    placeBuy(
        customerName,
        foodName,
        customer,
        food,
        total,
        Food,
        BaseIngredients,
        Warehouse,
        Trash,
        Customers,
        Restaurant,
        ManagerFile,
        Audit,
        Tax,
        command = '',
        Helper,
        wants,
        Statistic
    ) {
        //first of all we process food, reduce some qty from warehouse
        Warehouse.processFood(
            food,
            1,
            Food,
            BaseIngredients,
            false,
            Warehouse,
            Trash,
            Restaurant,
            ManagerFile,
            Audit,
            Tax,
            'Buy'
        );
        //then we add order to customer(for example, reduce customer budget)
        Customers.addOrder(
            customerName,
            foodName,
            customer,
            food,
            total,
            Restaurant,
            ManagerFile
        );
        //then we add money to restaurant budget and perform some tax calculations
        const taxMoney = Restaurant.addToBudget(
            wants,
            total,
            Warehouse,
            ManagerFile,
            Audit,
            Tax,
            'Budget',
            true
        );
        ManagerFile. writeOutputBuyInfo(
            customerName,
            foodName,
            Customers.getBudget(customer),
            total,
            'success, money: ' + taxMoney.money + ', tax: ' + taxMoney.tax
        );

        Statistic.addToProfitableDishes(foodName, total);

        // 6.11.1 Nazar_Kryvous Added wants quantity to tips

        const tips = Customers.checkTips(total, Helper);
        if (tips) {
            Restaurant.addToBudget(
                wants,
                tips,
                Warehouse,
                ManagerFile,
                Audit,
                Tax,
                command,
                false
            );
            Restaurant.addToDailyTips(tips, Tax);
            Customers.reduceFromBudget(customer, tips, true);
        }

        const log =
            customerName +
            ' - ' +
            foodName +
            ': success, money: ' +
            taxMoney.money +
            ', tax: ' +
            taxMoney.tax +
            ', tips: ' +
            tips;
        Audit.addItem(
            command,
            log,
            Warehouse.getWarehouse(),
            Restaurant.getBudgetInfo()
        );
        console.log(log);
        return log;
    }

    calculateBuyTotal(ingredients, BaseIngredients, Restaurant) {
        let total = BaseIngredients.getIngredientsPrice(ingredients);
        total = parseFloat((total * Restaurant.getMarkup()).toFixed(2));

        return parseFloat(total);
    }
}

module.exports = new ManagerBuy();
