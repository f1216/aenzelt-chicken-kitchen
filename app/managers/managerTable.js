/** This class is table command manager that perform all operation related to buying process for table
 * (like check different condition: customer/restaurant budget, spoiled ingredient or no, warehouse state)*/
class ManagerTable {
    processTable(
        data,
        Food,
        BaseIngredients,
        Warehouse,
        Trash,
        Customers,
        Restaurant,
        Manager,
        ManagerConditions,
        ManagerFile,
        ManagerBuy,
        Audit,
        Tax,
        Helper,
        Recommendations,
        Statistic
    ) {
        // First of all we have a lot of checkers here,
        // that is the first difference between table manager and buy manager
        if (!Array.isArray(data) || !data) return false;

        let pooled = false;
        if (data[0] === 'Pooled') {
            pooled = true;
            data.shift();
        }

        let customers = [];
        let dishes = [];

        // Here we create arrays with customers and dishes that we need to process table
        for (const item of data) {
            if (item.search(/Recommend/) !== -1) {
                dishes.push(item);
            } else {
                let itemArray = item.split(', ');
                for(let itemData of itemArray) {
                    if (Customers.getCustomerByName(itemData, false)) {
                        customers.push(itemData);
                    } else {
                        dishes.push(itemData);
                    }
                }
            }

        }

        // Another few checkers that give us info need we continue with command or no
        if (customers.length > dishes.length) {
            const log = 'Every person needs something to eat. Exit';
            ManagerFile.writeOutput(log);
            return false;
        } else if (customers.length < dishes.length) {
            const log = 'One person can have one type of food only. Exit';
            ManagerFile.writeOutput(log);
            return false;
        }

        let wantsQuantity = 0;
        let totalTable = 0;
        let items = [];

        // We foreach array of customers and dishes and process every item very close to buy process
        let previousCustomer = '';
        for (let i = 0; i < customers.length; i++) {
            if (customers[i] === previousCustomer) {
                const log =
                    'One person can appear only once at the table. Exit';
                ManagerFile.writeOutput(log);
                return false;
            }
            previousCustomer = customers[i];

            const customerName = customers[i];
            let foodName = dishes[i];

            if (dishes[i].search(/Recommend/) !== -1) {
                let ingredients = dishes[i].split(', ');
                ingredients.shift();
                foodName = Recommendations.getFoodWhichUserCanBuy(ingredients, Customers.getBudget(Customers.getCustomerByName(customerName)), BaseIngredients, Restaurant, Customers.getAllergies(Customers.getCustomerByName(customerName)), customerName, Audit, Food, Statistic, Warehouse);
                if (foodName === '') {
                    return false;
                }
            }


            const customer = Customers.getCustomerByName(customerName);
            const food = Food.getFoodByName(foodName);
            if (!customer || !food) return;
            const ingredients = Food.getIngredientsByFood(
                food,
                [],
                BaseIngredients
            );

            Statistic.addToPopularBaseIngredients(ingredients, BaseIngredients);

            // Nazar_Kryvous 6.11.1. I really want to eat something
            const wants = Customers.checkWants(Helper, BaseIngredients, Food, food);
            wantsQuantity += wants
            console.log(wants + "  -  Table Wants")


            const total = ManagerBuy.calculateBuyTotal(
                ingredients,
                BaseIngredients,
                Restaurant
            );
            totalTable += total;

            switch (
                ManagerConditions.conditionsChecks(
                    data,
                    customerName,
                    foodName,
                    customer,
                    food,
                    ingredients,
                    total,
                    Food,
                    BaseIngredients,
                    Warehouse,
                    Trash,
                    Customers,
                    Restaurant,
                    Manager,
                    ManagerFile,
                    Audit,
                    Tax,
                    'process table',
                    pooled,
                    Helper
                )
            ) {
                case 'ERROR_ALLERGEN':
                    return false;
                case 'ERROR_LOW_BUDGET':
                    return false;
                case 'ERROR_WAREHOUSE':
                    return false;
                case 'ERROR_SPOILED':
                    return false;
                case 'ERROR_NO_INGREDIENTS':
                    return false;
                case 'ALL_RIGHT':
                    items.push({
                        customerName: customerName,
                        foodName: foodName,
                        customer: customer,
                        food: food,
                        total: total,
                    });
                    break;
            }
        }

        this.placeTable(
            customers,
            dishes,
            items,
            totalTable,
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Restaurant,
            Customers,
            ManagerFile,
            Audit,
            Tax,
            'Table',
            pooled,
            Helper,
            wantsQuantity,
            Statistic
        );

        return true;
    }

    placeTable(
        customers,
        dishes,
        items,
        total,
        Food,
        BaseIngredients,
        Warehouse,
        Trash,
        Restaurant,
        Customers,
        ManagerFile,
        Audit,
        Tax,
        command,
        pooled = false,
        Helper,
        wantsQuantity,
        Statistic
    ) {

        //Pool checker(if one person can pay for other )
        if (pooled && !this.checkPooledAvailability(items, total, Customers))
            return false;
        const mainInfo = customers.concat(dishes).join(', ');
        let surcharge = 0;
        let surchargeTips = 0;
        let itemsInfo = [];
        for (const item of items) {
            // check if client can afford dish, if can`t calculate surcharge for rich person
            if (pooled) {
                if (!Customers.canAffordIt(item.customer, item.total)) {
                    const customerBudget = Customers.getBudget(item.customer);
                    surcharge += item.total - customerBudget;
                    item.total = customerBudget;
                }
            }

            //then process every item like in simple buy command
            Customers.addOrder(
                item.customerName,
                item.foodName,
                item.customer,
                item.food,
                item.total,
                Restaurant,
                ManagerFile
            );
            let tips = Customers.checkTips(item.total, Helper);
            if (tips && Customers.getBudget(item.customer) > tips) {
                Restaurant.addToBudget(
                    wantsQuantity,
                    tips,
                    Warehouse,
                    ManagerFile,
                    Audit,
                    Tax,
                    command,
                    false
                );
                Restaurant.addToDailyTips(tips, Tax);
                Customers.reduceFromBudget(item.customer, tips, true);
            } else if (tips && pooled) {
                //тут surcharge для тіпсів
                const customerBudget = Customers.getBudget(item.customer);
                surchargeTips += tips - customerBudget;
                tips = customerBudget;
                Restaurant.addToBudget(
                    wantsQuantity,
                    tips,
                    Warehouse,
                    ManagerFile,
                    Audit,
                    Tax,
                    command,
                    false
                );

                Restaurant.addToDailyTips(tips, Tax);
                Customers.reduceFromBudget(item.customer, tips, true);
            }
            itemsInfo.push(
                item.customerName +
                    ', ' +
                    Customers.getBudget(item.customer) +
                    ', ' +
                    item.foodName +
                    ', ' +
                    item.total
            );
        }

        if (surcharge !== 0) {
            if (
                !this.processSurcharge(items, Customers, surcharge, ManagerFile)
            ) {
                itemsInfo = itemsInfo.join('\r\n');
                ManagerFile.writeOutputTableInfo(
                    mainInfo,
                    itemsInfo,
                    total,
                    'failed, table have low budget, need to surcharge ' +
                        surcharge,
                    'Table'
                );

                const log =
                    'failed, table have low budget, need to surcharge ' +
                    surcharge;
                Audit.addItem(
                    command,
                    log,
                    Warehouse.getWarehouse(),
                    Restaurant.getBudgetInfo()
                );
                console.log('Table, ' + log);
                return false;
            }
        }
        if (surchargeTips !== 0) {
            this.processSurcharge(items, Customers, surchargeTips, ManagerFile);
        }


        ///Цей блок був на початку методу. але я вирішив перенести його, щоб
        dishes.map((item) => {
            Warehouse.processFood(
                item,
                1,
                Food,
                BaseIngredients,
                false,
                Warehouse,
                Trash,
                Restaurant,
                ManagerFile,
                Audit,
                Tax,
                command
            );

            Statistic.addToProfitableDishes(item, BaseIngredients.getIngredientsPrice(Food.getIngredientsByFood(Food.getFoodByName(item), [], BaseIngredients)));
        });

        const taxMoney = Restaurant.addToBudget(
            wantsQuantity,
            total,
            Warehouse,
            ManagerFile,
            Audit,
            Tax,
            'Budget',
            true
        );

        itemsInfo = itemsInfo.join('\r\n');
        ManagerFile.writeOutputTableInfo(
            mainInfo,
            itemsInfo,
            total,
            'success, money: ' + taxMoney.money + ', tax: ' + taxMoney.tax,
            'Table'
        );
        const log =
            mainInfo +
            ' -> success, money: ' +
            taxMoney.money +
            ', tax: ' +
            taxMoney.tax;
        Audit.addItem(
            command,
            log,
            Warehouse.getWarehouse(),
            Restaurant.getBudgetInfo()
        );
        console.log('Table, ' + log);
    }

    checkPooledAvailability(items, total, Customers) {
        let totalCustomersBudget = 0;
        for (const item of items) {
            totalCustomersBudget += Customers.getBudget(item.customer);
        }
        if (total > totalCustomersBudget) return false;
        return true;
    }

    /** Method that perform surcharging from "rich" person an surcharge */
    processSurcharge(items, Customers, surcharge, ManagerFile) {
        for (const item of items) {
            let paid = 0;
            if (!Customers.canAffordIt(item.customer, surcharge)) {
                const customerBudget = Customers.getBudget(item.customer);
                surcharge -= customerBudget;
                Customers.reduceFromBudget(item.customer, customerBudget);
                paid = customerBudget;
                //моожна додати логи про те, скільки і з кого знялося додаткової оплати
            } else {
                Customers.reduceFromBudget(item.customer, surcharge);
                paid = surcharge;
                surcharge = 0;
                //моожна додати логи про те, скільки і з кого знялося додаткової оплати
            }
            if (paid > 0) {
                const log =
                    'Customer ' +
                    item.customerName +
                    ' paid: ' +
                    paid +
                    ' surcharge for table';
                ManagerFile.writeOutput(log);
                console.log(log);
            }
        }
        if (surcharge !== 0) return false;
        return true;
    }


}

module.exports = new ManagerTable();
