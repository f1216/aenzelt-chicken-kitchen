/** This class is budget command manager that call 3 base command for budget*/
class ManagerBudget {
    processBudget(data, Warehouse, Restaurant, ManagerFile, Audit, Tax) {
        if (!Array.isArray(data) || !data) return false;

        const operation = data[0];
        const total = data[1];

        switch (operation) {
            case '+':
                Restaurant.addToBudget(
                    total,
                    Warehouse,
                    ManagerFile,
                    Audit,
                    Tax,
                    'Budget'
                );
                break;
            case '-':
                Restaurant.reduceFromBudget(
                    total,
                    Warehouse,
                    ManagerFile,
                    Audit,
                    'Budget'
                );
                break;
            case '=':
                Restaurant.setBudgetBOSS(
                    total,
                    Warehouse,
                    ManagerFile,
                    Audit,
                    'Budget'
                );
                break;
            default:
                return  false;
        }

        return true;
    }
}
module.exports = new ManagerBudget();
