/** This class is order manager, here we process order command*/
class ManagerOrder {
    processOrder(
        data,
        BaseIngredients,
        Food,
        Warehouse,
        Trash,
        Restaurant,
        ManagerFile,
        Audit,
        Tax,
        Helper
    ) {
        if (!Array.isArray(data) || !data) return false;

        // create array with orders ingredient,
        // that algorithm work properly both if we want to order one ingredient or two and more
        let perChunk = 2; // items per chunk

        let ordersArray = data.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);
            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = []; // start a new chunk
            }
            resultArray[chunkIndex].push(item);
            return resultArray;
        }, []);
        //

        for (const data of ordersArray) {
            const ingredient = data[0];
            const qty = data[1];

            if (
                !Restaurant.orderIngredient(
                    ingredient,
                    qty,
                    BaseIngredients,
                    Food,
                    Warehouse,
                    Trash,
                    Restaurant,
                    ManagerFile,
                    Audit,
                    Tax,
                    'Order',
                    Helper
                )
            ) {
                return false;
            }
        }
    }
}

module.exports = new ManagerOrder();
