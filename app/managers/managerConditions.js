class ManagerConditions {
    conditionsChecks(
        data,
        customerName,
        foodName,
        customer,
        food,
        ingredients,
        total,
        Food,
        BaseIngredients,
        Warehouse,
        Trash,
        Customers,
        Restaurant,
        Manager,
        ManagerFile,
        Audit,
        Tax,
        command = '',
        pooled = false,
        Helper,
        checkSpoiled = true
    ) {
        let returnData = false;
        let log = '';

        const allergen = Customers.checkAllergen(
            customer,
            ingredients,
            BaseIngredients
        );
        if (allergen) {
            if (Warehouse.getConfCommandsWithReduce().includes(command)) {
                switch (Warehouse.getConfDishesWithAllergies()) {
                    case 'waste':
                        Warehouse.processFood(
                            food,
                            1,
                            Food,
                            BaseIngredients,
                            false,
                            Warehouse,
                            Trash,
                            Restaurant,
                            ManagerFile,
                            Audit,
                            Tax,
                            command
                        );
                        break;
                    case 'keep':
                        if (
                            Warehouse.checkAvailability(
                                food,
                                1,
                                Food,
                                BaseIngredients,
                                true
                            )
                        ) {
                            Warehouse.processFood(
                                food,
                                1,
                                Food,
                                BaseIngredients,
                                true,
                                Warehouse,
                                Trash,
                                Restaurant,
                                ManagerFile,
                                Audit,
                                Tax,
                                command
                            );
                        }
                        break;
                    default:
                        let allergies = false;
                        if (total > Warehouse.getConfDishesWithAllergies()) {
                            allergies = true;
                        }
                        Warehouse.processFood(
                            food,
                            1,
                            Food,
                            BaseIngredients,
                            allergies,
                            Warehouse,
                            Trash,
                            Restaurant,
                            ManagerFile,
                            Audit,
                            Tax,
                            command
                        );
                        break;
                }
            }
            ManagerFile.writeOutputBuyInfo(
                customerName,
                foodName,
                Customers.getBudget(customer),
                total,
                'can’t order, allergic to: ' + allergen,
                command
            );
            let log =
                customerName +
                ' - ' +
                foodName +
                ': can’t order, allergic to: ' +
                allergen;

            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            console.log(log);

            returnData = 'ERROR_ALLERGEN';
        }
        if (!Customers.canAffordIt(customer, total) && !returnData) {
            ManagerFile.writeOutputBuyInfo(
                customerName,
                foodName,
                Customers.getBudget(customer),
                total,
                'can’t order, low budget'
            );
            let log =
                customerName +
                ': can’t order, budget ' +
                Customers.getBudget(customer) +
                ' and ' +
                foodName +
                ' cost ' +
                total;

            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            console.log(log);

            if (!pooled) {
                returnData = 'ERROR_LOW_BUDGET';
            }
        }
        if (
            !Warehouse.checkAvailability(
                food,
                1,
                Food,
                BaseIngredients,
                true
            ) &&
            !returnData
        ) {
            log =
                customerName +
                ' - ' +
                foodName +
                ': can’t ' +
                command +
                ', no ingredients';

            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            console.log(log);

            returnData = 'ERROR_NO_INGREDIENTS';
        }
        if (
            BaseIngredients.checkSpoiled(
                ingredients,
                Trash,
                Warehouse,
                ManagerFile,
                Audit,
                Tax,
                command,
                Helper
            ) &&
            !returnData && checkSpoiled
        ) {
            log =
                customerName +
                ' - ' +
                foodName +
                ' - ' +
                command +
                ', spoiled ingredients';

            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            console.log(log);

            return this.conditionsChecks(data,
                customerName,
                foodName,
                customer,
                food,
                ingredients,
                total,
                Food,
                BaseIngredients,
                Warehouse,
                Trash,
                Customers,
                Restaurant,
                Manager,
                ManagerFile,
                Audit,
                Tax,
                command,
                pooled,
                Helper,
                false);
        }

        if (returnData) {
            if (command === 'Table') {
                log =
                    command + ', ' + data.join(', ') + ' -> failed;\r\n' + log;
                ManagerFile.writeOutput(log);
            }
            return returnData;
        }

        return 'ALL_RIGHT';
    }
}

module.exports = new ManagerConditions();
