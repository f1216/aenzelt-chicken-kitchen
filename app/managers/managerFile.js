const csvWriter = require('csv-writer');
const fs = require('fs');
const conf = require('../../conf');
const Restaurant = require('../restaurant');
/** This class is file manager, here we process different kind of operations with files
 * read/write, preprocess some file */
class ManagerFile {
    load(Restaurant) {
        this.writeOutput(Restaurant.getBudgetInfo());
    }

    upload(Tax) {
        this.writeOutput(Restaurant.getBudgetInfo());
        this.writeOutput(Restaurant.getDailyProfitInfo(Tax));
        this.writeOutput(
            Tax.getDailyTaxInfo(Restaurant) +
                '\r\n' +
                Tax.getDailyTipsTaxInfo() +
                '\r\n' +
                Tax.getDailyTrashTaxInfo()
        );
    }

    /** Method for preparing commands from input file,
     * would be good if this method
     * and next method check command data into main manager file for example*/
    getCommandListFromInput(fileName = '') {
        let commandList = [];
        let list = '';
        try {
            list = fs.readFileSync(conf.input_dir + fileName, {
                encoding: 'utf8',
                flag: 'r',
            });
        } catch (e) {
            list = '';
        }

        list = list.split('\r\n');
        for (let item of list) {


            let commandData = [];
            if (item.search(/\(Recommend/) !== -1) {
                commandData = item.split(', (');
            } else {
                commandData = item.split(', ');
            }

            if (!this.checkCommandData(commandData)) continue;
            const command = commandData.shift();
            commandList.push({ command: command, data: commandData });
        }

        return commandList;
    }

    checkCommandData(orderData = []) {
        if (!Array.isArray(orderData)) {
            return false;
        }
        for (let i = 0; i < orderData.length; i++) {
            orderData[i] = orderData[i].replace(')', '');
            if (orderData[i] === undefined || orderData[i].trim() === '')
                return false;
        }
        if (conf.allowed_command_list[orderData[0].trim()] === undefined)
            return false;

        return true;
    }

    write(string = '', path, flag = 'a+') {
        !string ? (string = '') : string;
        string = string.trim('\r\n') + '\r\n';
        try {
            fs.writeFileSync(path, string, {
                encoding: 'utf8',
                flag: flag,
                mode: 0o666,
            });
        } catch (e) {
            return false;
        }
    }

    writeOutput(string = '') {
        !string ? (string = '') : string;
        string = string.trim('\r\n') + '\r\n';
        try {
            fs.writeFileSync(conf.output_dir + conf.log_filename, string, {
                encoding: 'utf8',
                flag: 'a+',
                mode: 0o666,
            });
        } catch (e) {
            return false;
        }
    }

    /** Below we have some functions for writing our main output file,
     * with some prepared structure */

    writeOutputBuyInfo(
        customerName,
        foodName,
        budget,
        total,
        result,
        command = 'Buy'
    ) {
        const orderInfo =
            command +
            ', ' +
            customerName +
            ', ' +
            foodName +
            ' -> ' +
            customerName +
            ', ' +
            budget +
            ', ' +
            foodName +
            ', ' +
            total +
            ' -> ' +
            result;
        this.writeOutput(orderInfo);
    }

    writeOutputTableInfo(
        mainInfo,
        itemsInfo,
        total,
        result,
        command = 'Total'
    ) {
        const orderInfo =
            command +
            ', ' +
            mainInfo +
            ' -> ' +
            result +
            '; money amount: ' +
            total +
            '\r\n' +
            '{\r\n' +
            itemsInfo +
            '\r\n}\r\n';
        this.writeOutput(orderInfo);
    }

    async rewriteData(columns = [], records, fileName) {
        records.map((item) => {
            columns = Object.keys(item);
        });
        let header = [];
        [];
        for (let i = 0; i < columns.length; i++)
            header.push({ id: columns[i], title: columns[i] });

        // columns.map(item => header.push({id: item, title: item}));
        const csvWriterObj = csvWriter.createObjectCsvWriter({
            path: fileName,
            header: header,
        });

        await csvWriterObj.writeRecords(records);
    }
}

module.exports = new ManagerFile();
