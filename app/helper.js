/** This class is helper, which contain different helpers for our application like methods for randomize*/
class Helper {
    getRandomChance() {
        return parseFloat(Math.random().toFixed(1));
    }

    getRandomChanceTips() {
        return parseFloat(Math.random().toFixed(1));
    }

    getRandomChanceVolatility() {
        return parseFloat(Math.random().toFixed(1));
    }

    getRandomChanceWants() {
        let randomWant = Math.floor(Math.random() * 101)
        return randomWant;
    }

    getRandomChanceTime() {
        return parseFloat(Math.random().toFixed(1));
    }
}

module.exports = new Helper();
