const conf = require('../conf');

class Tax {
    dailyTransactionTax = 0;
    dailyTipsTax = 0;
    dailyTrashTax = 0;

    getConfDailyTax() {
        return conf.daily_tax;
    }

    getDailyTransactionTax() {
        return this.dailyTransactionTax;
    }

    addToDailyTransactionTax(dailyTransactionTax) {
        dailyTransactionTax = parseFloat(
            parseFloat(dailyTransactionTax).toFixed(2)
        );
        if (isNaN(dailyTransactionTax)) return false;
        this.dailyTransactionTax += dailyTransactionTax;
    }

    getConfTipsTax() {
        return conf.daily_tips_tax;
    }

    getDailyTipsTax() {
        return this.dailyTipsTax;
    }

    getDailyTipsTaxInfo() {
        return 'Daily tips tax ' + this.getDailyTipsTax();
    }

    addToDailyTipsTax(tax) {
        tax = parseFloat(tax.toFixed(2));
        if (isNaN(tax)) return false;
        this.dailyTipsTax += tax;
    }

    getConfTrashTax() {
        return conf.trash_tax;
    }

    getConfTrashTaxLimiter() {
        return conf.trash_tax_limiter;
    }

    getDailyTrashTax() {
        return this.dailyTrashTax;
    }

    getDailyTrashTaxInfo() {
        return 'Daily trash tax ' + this.getDailyTrashTax();
    }

    addToDailyTrashTax(tax) {
        tax = parseFloat(tax.toFixed(2));
        if (isNaN(tax)) return false;
        if (
            this.getDailyTrashTax() >= this.getConfTrashTaxLimiter() ||
            this.getDailyTrashTax() + tax > this.getConfTrashTaxLimiter()
        ) {
            tax += 20;
        }
        this.dailyTrashTax += tax;
    }

    getDailyTax(Restaurant) {
        let dailyTax =  parseFloat(
            (Restaurant.getDailyProfit(this) * this.getConfDailyTax()).toFixed(
                2
            )
        );

        if (dailyTax > 0) {
            return dailyTax;
        } else return 0;
    }

    getDailyTaxInfo(Restaurant) {
        return 'Daily tax ' + this.getDailyTax(Restaurant);
    }

    getConfTax() {
        return conf.transaction_tax;
    }
}

module.exports = new Tax();
