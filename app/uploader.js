const ManagerFile = require('./managers/managerFile');
const Warehouse = require('./lib/warehouse');
const Customers = require('./lib/customers');
const Statistic = require('./lib/statistic');
const BaseIngredients = require('./lib/baseIngredients');
const Tax = require('./tax');

class Uploader {
    upload() {
        Warehouse.upload(ManagerFile);
        ManagerFile.upload(Tax);
        Customers.upload(ManagerFile);
        Statistic.upload(ManagerFile, BaseIngredients);
    }
}

module.exports = new Uploader();
