const conf = require('../conf');

/** This class is main restaurant class, here we have methods for main structures and operations in restaurant,
 * such a budget operations or operations for ordering
 * (maybe it would be great if we can do some refactoring                                                                                                                                                                                                            later
 * and transfer some part of budget or ordering logic into another classes)*/
class Restaurant {
    dayBudgetStart = 0;
    budget = 0;
    dailyTips = 0;

    load() {
        this.dayBudgetStart = this.budget = this.getConfBudget();
    }

    getDayBudgetStart() {
        return this.dayBudgetStart;
    }

    getBudget() {
        return this.budget;
    }

    getConfBudget() {
        return conf.budget;
    }

    getDailyTips() {
        return this.dailyTips;
    }

    addToDailyTips(tips, Tax) {
        tips = parseFloat(parseFloat(tips).toFixed(2));
        if (isNaN(tips)) return false;
        this.dailyTips += tips;
        const tax = parseFloat((tips * Tax.getConfTipsTax()).toFixed(2));
        Tax.addToDailyTipsTax(tax);
    }

    getDailyProfit(Tax) {
        let profit = parseFloat(
            (
                this.getBudget() -
                this.getDayBudgetStart() -
                Tax.getDailyTransactionTax()
            ).toFixed(2)
        );
        if (profit > 0) {
            return profit;
        } else return 0;
    }

    getDailyProfitInfo(Tax) {
        return 'Daily profit ' + this.getDailyProfit(Tax);
    }

    getConfOrderCommand() {
        return conf.commands_settings.Order;
    }

    setBudget(budget) {
        budget = parseFloat(parseFloat(budget).toFixed(2));
        if (isNaN(budget)) return false;
        this.budget = parseFloat(budget);
    }

    setBudgetBOSS(budget, Warehouse, ManagerFile, Audit, command) {
        this.setBudget(budget);
        const log = 'BOSS set restaurant budget to ' + budget;
        console.log(log);
        ManagerFile.writeOutput(log);
        Audit.addItem(
            command,
            log,
            Warehouse.getWarehouse(),
            this.getBudgetInfo()
        );
    }

    // 6.11.1 Nazar_Kryvous Added wants quantity to tips
    addToBudget(
        wants,
        total,
        Warehouse,
        ManagerFile,
        Audit,
        Tax,
        command,
        calculateProfitTax
    ) {
        //6.11.1 Nazar_Kryvous
        (!calculateProfitTax) ? (total = parseFloat(total) * Math.pow(2, wants)) : total = parseFloat(total);
        // total = parseFloat(total) * Math.pow(2, wants);
        if (isNaN(total)) return false;
        let tax = 0;
        if (calculateProfitTax) {
            tax = total * Tax.getConfTax();
            Tax.addToDailyTransactionTax(tax);
            total = total - tax;
        }

        this.setBudget(this.getBudget() + total);
        const log =
            'Add ' + parseFloat(total.toFixed(2)) + ' to restaurant budget';
        console.log(log);
        ManagerFile.writeOutput(log);
        Audit.addItem(
            command,
            log,
            Warehouse.getWarehouse(),
            this.getBudgetInfo()
        );

        return {
            tax: parseFloat(tax.toFixed(2)),
            money: parseFloat(total.toFixed(2)),
        };
    }

    reduceFromBudget(total, Warehouse, ManagerFile, Audit, command) {
        if (this.getBudget() < 0) return false;
        total = parseFloat(parseFloat(total).toFixed(2));
        if (isNaN(total)) return false;

        this.setBudget(this.getBudget() - total);
        const log = 'Reduce ' + total.toFixed(2) + ' from restaurant budget';
        console.log(log);
        ManagerFile.writeOutput(log);
        Audit.addItem(
            command,
            log,
            Warehouse.getWarehouse(),
            this.getBudgetInfo()
        );
        return this.getBudget();
    }

    getBudgetInfo() {
        let info = '';
        if (this.getBudget() <= 0) {
            info = 'Restaurant - bankrupt';
        } else {
            info = 'Restaurant budget: ' + this.getBudget();
        }
        return info;
    }

    getMarkup() {
        return conf.markup;
    }

    getConfVolatilityDish() {
        return conf.volatility_dish;
    }

    getConfVolatilityIngredient() {
        return conf.volatility_ingredient;
    }

    getVolatility(baseIngredient, Helper) {
        let volatility = this.getConfVolatilityIngredient();
        if (!baseIngredient) volatility = this.getConfVolatilityDish();

        if (Helper.getRandomChanceVolatility() > 0.5) {
            volatility = 1 + volatility;
        } else {
            volatility = 1 - volatility;
        }
        return volatility;
    }

    getDiscount() {
        return conf.every_third_discount;
    }

    getDiscountPercent() {
        return conf.every_third_discount * 100;
    }

    orderIngredient(
        ingredient,
        qty,
        BaseIngredients,
        Food,
        Warehouse,
        Trash,
        Restaurant,
        ManagerFile,
        Audit,
        Tax,
        command,
        Helper
    ) {
        if (this.getConfOrderCommand() === 'No') {
            return false;
        }
        qty = parseInt(qty);
        const baseIngredient = BaseIngredients.isBaseIngredient(ingredient);
        if (!baseIngredient && (this.getConfOrderCommand() !== 'All' && this.getConfOrderCommand() !== 'Dishes')) {
            const log =
                'Restaurant can not order ' +
                ingredient +
                ' it`s not a base ingredient';
            console.log(log);
            ManagerFile.writeOutput(log);
            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            return false;
        }
        if (baseIngredient && this.getConfOrderCommand() === 'Dishes') {
            const log =
                'Restaurant can not order ' +
                ingredient +
                ' it`s not a base ingredient';
            console.log(log);
            ManagerFile.writeOutput(log);
            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            return false;
        }
        if (
            !Warehouse.checkWarehouseQtyConf(
                ingredient,
                qty,
                Warehouse.getItemByName(ingredient),
                BaseIngredients,
                Food,
                Warehouse,
                Trash,
                Restaurant,
                ManagerFile,
                Audit,
                Tax,
                command
            )
        ) {
            return false;
        }

        if (
            BaseIngredients.isSpoiled(
                ingredient,
                qty,
                Restaurant,
                Warehouse,
                ManagerFile,
                Audit,
                Tax,
                command,
                Helper
            )
        ) {
            const log = 'Can’t ' + command + ', spoiled ingredients';
            ManagerFile.writeOutput(log);
            Audit.addItem(
                command,
                log,
                Warehouse.getWarehouse(),
                Restaurant.getBudgetInfo()
            );
            console.log(log);
            return false;
        }

        const reduceBase =
            BaseIngredients.getPrice(baseIngredient) * parseInt(qty);
        const tax = reduceBase * Tax.getConfTax();
        Tax.addToDailyTransactionTax(tax);
        const reduceFinal =
            (reduceBase + tax) * this.getVolatility(baseIngredient, Helper);
        this.reduceFromBudget(
            reduceFinal,
            Warehouse,
            ManagerFile,
            Audit,
            command
        );
        Warehouse.addQty(Warehouse.getItemByName(ingredient), qty);
        let log =
            'Restaurant ordered ' +
            ingredient +
            ' x' +
            qty +
            ', tax: ' +
            tax +
            ', spend: ' +
            reduceFinal;
        console.log(log);
        ManagerFile.writeOutput(log);
        Audit.addItem(
            command,
            log,
            Warehouse.getWarehouse(),
            Restaurant.getBudgetInfo()
        );
        return log;
    }
}

module.exports = new Restaurant();
