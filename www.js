const express = require('express');
const Process = require('./api/process');
const Output = require('./api/io/output');
const Conf = require('./api/conf');
const Loader = require('./app/loader');
const Customers = require('./api/customers');
const cors = require('cors');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const app = express();
const port = 3001;
app.use(cors());
app.use(fileUpload());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Chicken-Kitchen!');
});
app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});

//load data for server app
Loader.load();

app.use('/api/conf/', Conf);
app.use('/api/process/', Process);
app.use('/api/output/', Output);
app.use('/api/customers/', Customers);