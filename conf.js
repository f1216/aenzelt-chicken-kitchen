const conf = {};

conf.customers_csv_path = './data/regular_customer_allergies.csv';
conf.food_csv_path = './data/food_ingredients.csv';
conf.base_ingredients_csv_path = './data/base_ingredients.csv';
conf.warehouse_csv_path = './data/warehouse.csv';
conf.popular_ingredients = './data/popular_ingredients.json';
conf.popular_dishes = './data/popular_dishes.json';

conf.input_dir = './input/';
conf.output_dir = './output/';
conf.log_filename = 'output_log';
conf.audit_filename = 'audit';
conf.trash_pull_filename = 'trash_pull';
conf.base_ingredient_statistic = 'base_ingredient_statistic.json';

conf.budget = 500000;
conf.markup = 1.4;
conf.transaction_tax = 0.1;
conf.daily_tax = 0.1;
conf.daily_tips_tax = 0.05;
conf.trash_tax = 0.15;
conf.trash_tax_limiter = 100;
// Task 6.10
conf.tips_tax = 0.05;

conf.allowed_command_list = {
    Buy: true,
    Budget: true,
    Order: true,
    Table: true,
    Audit: true,
    Delay: true,
    'Throw trash away': true,
};

conf.default_item_qty = 5; //5 by task

conf.warehouse = {
    total_max: 200,
    total_dishes_max: 3,
    total_ingredients_max: 10,
    dishes_with_allergies: 100, //keep, waste, number
    commands_with_reduce: ['Buy'],
};

conf.commands_settings = {
    Order: 'All',
};

conf.every_third_discount = 0.1;
conf.allergies_percent = 0.25;

conf.spoil_chance = 0.01;

conf.tresh_max = 500;

conf.max_tips_amount = 50;
conf.tips_percent = 0.6;

conf.volatility_dish = 0.25;
conf.volatility_ingredient = 0.1;

conf.base_time_for_buy = 5;
conf.base_time_for_table_person = 1.05;
conf.base_time_for_order_total = 10;
conf.base_time_for_order = 1000;
conf.base_time_for_order_ingredient = 1.05;
conf.base_time_for_order_dish = 1.1;

module.exports = conf;
