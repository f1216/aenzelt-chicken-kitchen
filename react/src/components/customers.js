import '../css/customers.css';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import { createStyles, makeStyles } from '@mui/styles';
import { DataGrid, GridActionsCellItem} from '@mui/x-data-grid';
import { AiFillDelete} from 'react-icons/ai';
import { MdOutlineSystemUpdateAlt } from 'react-icons/md';
import {Button} from "@mui/material";

function Customers() {
    const [rows, setRows] = useState([]);
    const [columns, setColumns] = useState([]);
    const [name, setName] = useState('');
    const [allergies, setAllergies] = useState('');
    const [budget, setBudget] = useState(0);

    function addCustomer() {

    }

    function updateCustomer() {

    }

    function deleteCustomer() {

    }

    useEffect(() => {
        fetch('http://localhost:3001/api/customers/getCustomers')
            .then((res) => res.json())
            .then((data) => {
                let columns = [];
                Object.keys(data[0]).map((item) => {
                    columns.push({
                        field: item,
                        width: 300,
                        editable: true,
                        renderCell: renderCellExpand
                    });
                    return true;
                });
                columns.push(
                    {
                        field: 'update',
                        renderCell: (params) => {
                            return <Button onClick={() => {
                                updateCustomer();
                            }}>Update</Button>;
                        }
                    },
                    {
                        field: 'delete',
                        renderCell: (params) => {
                            return <Button onClick={() => {
                                    deleteCustomer();
                                }}>Delete</Button>;
                        }
                    }
                );
                setColumns(columns);
                let i = 0;
                data.map((item) => {
                    item.id = i;
                    i++;
                    return true;
                });
                setRows(data);
            });
    }, []);

    return (
        <div className="Customers">
            <div className="AddCustomer">
                <div className="AddCustomerTextInput">
                    <p>Add name:</p>
                    <input
                        type="text"
                        name="name"
                        onChange={(e) => {
                            console.log(e.target.value);
                            setName(e.target.value);
                        }}
                    />
                </div>
                <div className="AddCustomerTextInput">
                    <p>Add allergies:</p>
                    <input
                        type="text"
                        name="allergies"
                        onChange={(e) => {
                            console.log(e.target.value);
                            setAllergies(e.target.value);
                        }}
                    />
                </div>
                <div className="AddCustomerTextInput">
                    <p>Add budget:</p>
                    <input
                        type="text"
                        name="budget"
                        onChange={(e) => {
                            console.log(e.target.value);
                            setBudget(e.target.value);
                        }}
                    />
                </div>
                <div className="AddCustomerSubmit">
                    <button
                        type="submit"
                        onClick={() => {
                            addCustomer();
                        }}
                    >Add customer
                    </button>
                </div>
            </div>
            <div className="CustomersGrid">
                <DataGrid rows={rows} columns={columns} />
            </div>
        </div>
    );
}

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            alignItems: 'center',
            lineHeight: '24px',
            width: '100%',
            height: '100%',
            position: 'relative',
            display: 'flex',
            '& .cellValue': {
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
            },
        },
    }),
);

function isOverflown(element) {
    return (
        element.scrollHeight > element.clientHeight ||
        element.scrollWidth > element.clientWidth
    );
}

const GridCellExpand = React.memo(function GridCellExpand(props) {
    const { width, value } = props;
    const wrapper = React.useRef(null);
    const cellDiv = React.useRef(null);
    const cellValue = React.useRef(null);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const classes = useStyles();
    const [showFullCell, setShowFullCell] = React.useState(false);
    const [showPopper, setShowPopper] = React.useState(false);

    const handleMouseEnter = () => {
        const isCurrentlyOverflown = isOverflown(cellValue.current);
        setShowPopper(isCurrentlyOverflown);
        setAnchorEl(cellDiv.current);
        setShowFullCell(true);
    };

    const handleMouseLeave = () => {
        setShowFullCell(false);
    };

    React.useEffect(() => {
        if (!showFullCell) {
            return undefined;
        }

        function handleKeyDown(nativeEvent) {
            // IE11, Edge (prior to using Bink?) use 'Esc'
            if (nativeEvent.key === 'Escape' || nativeEvent.key === 'Esc') {
                setShowFullCell(false);
            }
        }

        document.addEventListener('keydown', handleKeyDown);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [setShowFullCell, showFullCell]);

    return (
        <div
            ref={wrapper}
            className={classes.root}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        >
            <div
                ref={cellDiv}
                style={{
                    height: 1,
                    width,
                    display: 'block',
                    position: 'absolute',
                    top: 0,
                }}
            />
            <div ref={cellValue} className="cellValue">
                {value}
            </div>
            {showPopper && (
                <Popper
                    open={showFullCell && anchorEl !== null}
                    anchorEl={anchorEl}
                    style={{ width, marginLeft: -17 }}
                >
                    <Paper
                        elevation={1}
                        style={{ minHeight: wrapper.current.offsetHeight - 3 }}
                    >
                        <Typography variant="body2" style={{ padding: 8 }}>
                            {value}
                        </Typography>
                    </Paper>
                </Popper>
            )}
        </div>
    );
});

GridCellExpand.propTypes = {
    value: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
};

function renderCellExpand(params) {
    return (
        <GridCellExpand value={params.value || ''} width={params.colDef.computedWidth} />
    );
}

export default Customers;
