import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import '../css/commands.css';
import 'react-awesome-button/dist/styles.css';
const axios = require('axios');

function Commands({ stateChanger, ...rest }) {
    function processInput() {
        console.log({
            command: command,
            paramsInput: paramsInput,
            fileInput: fileInput,
        });
        const formData = new FormData();
        formData.append('command', command);
        formData.append('paramsInput', paramsInput);
        formData.append('file', fileInput); // appending file
        axios(
            // '/api/process/processInput',
            //     {command: command, paramsInput: paramsInput},
            {
                method: 'post',
                url: '/api/process/processInput',
                data: formData,
                headers: { 'Content-Type': 'application/json;charset=UTF-8' },
            }
        )
            .then((response) => {
                console.log(stateChanger);
                stateChanger('yes');
            })
            .catch((error) => {});
    }

    const [commandsList, setCommandsList] = useState([]);
    const [command, setCommand] = useState('');
    const [paramsInput, setParamsInput] = useState('');
    const [fileInput, setFileInput] = useState('');

    useEffect(() => {
        fetch('/api/conf/getCommands')
            .then((res) => res.json())
            .then((data) => {
                let commands = [];
                data.map((item) => {
                    commands.push({ value: item, label: item });
                    return true;
                });
                setCommandsList(commands);
            });
    }, []);

    return (
        <div className="Commands">
            <div className="CommandsList">
                <p>Select command here:</p>
                <Select
                    className="CommandsListSelect"
                    options={commandsList}
                    onChange={(e) => {
                        console.log(e.value);
                        setCommand(e.value);
                    }}
                />
            </div>
            <div className="CommandsInput">
                <div className="CommandsParams">
                    <p>Add params:</p>
                    <input
                        className="CommandsParamsInput"
                        type="text"
                        name="paramsInput"
                        onChange={(e) => {
                            console.log(e.target.value);
                            setParamsInput(e.target.value);
                        }}
                    />
                </div>
                <div className="CommandsFile">
                    <p>OR use file</p>
                    <input
                        type="file"
                        name="file"
                        onChange={(e) => {
                            setFileInput(e.target.files[0]);
                        }}
                    />
                </div>
                <div className="CommandsSubmit">
                    <button
                        type="submit"
                        onClick={() => {
                            processInput();
                        }}
                    >
                        Process Commands
                    </button>
                </div>
            </div>
        </div>
    );
}
export default Commands;
