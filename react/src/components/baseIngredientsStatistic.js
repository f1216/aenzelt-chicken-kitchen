import '../css/statistic.css';
import React, { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import axios from "axios";
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);


function BaseIngredientsStatistic() {
    const [statistic, setStatistic] = useState([]);

    useEffect(() => {
        function fetchStatistic()  {
            fetch('http://localhost:3001/api/output/getBaseIngredientsStatistic')
                .then((res) => res.json())
                .then((data) => {
                    const labels = data.labels;
                    const dataSets = data.dataSets;
                    const bis = document.getElementById('bis');
                    const myChart = new Chart(bis, {
                        type: 'line',
                        data: {
                            labels: labels,
                            datasets: dataSets
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });
                    setStatistic(data);
                });
        }
        fetchStatistic();
    }, []);

    return (
        <div className="Statistic">
            <canvas id="bis" width="200" height="200"></canvas>
        </div>
    );
}
export default BaseIngredientsStatistic;
