import '../css/output.css';
import React, { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';

function Output() {
    const [rows, setRows] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3001/api/output/getLog')
            .then((res) => res.json())
            .then((data) => {
                setRows(data);
            });
    }, []);

    const columns = [{ field: 'log', headerName: 'Output log', width: 700 }];

    return (
        <div className="Output">
            <DataGrid rows={rows} columns={columns} />
        </div>
    );
}
export default Output;
