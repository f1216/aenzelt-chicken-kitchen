import React, { useState } from 'react';
import Output from './output';
import Customers from './customers';
import BaseIngredients from './baseIngredients';
import Food from './food';
import Warehouse from './warehouse';
import Trash from './trash';
import '../css/tabs.css';
import BaseIngredientsStatistic from "./baseIngredientsStatistic";

function Tabs() {
    const componentsList = [
        { name: 'Output', component: <Output /> },
        { name: 'Customers', component: <Customers /> },
        { name: 'BaseIngredients', component: <BaseIngredients /> },
        { name: 'Food', component: <Food /> },
        { name: 'Warehouse', component: <Warehouse /> },
        { name: 'Trash', component: <Trash /> },
        { name: 'Statistic', component: <BaseIngredientsStatistic /> },
    ];

    const [activeTabComponent, setActiveTabComponent] = useState(
        componentsList[0].component
    );
    const [activeTabsMenuItem, setActiveTabsMenuItem] = useState(
        componentsList[0].name
    );

    let buttons = [];
    componentsList.map((item) => {
        let style = { width: 100 / componentsList.length + '%' };
        if (item.name === activeTabsMenuItem) {
            style = {
                width: 100 / componentsList.length + '%',
                'background-color': '#DCDCDC',
                'border-left-color': 'hsl(359, 92%, 60%)',
                'border-right-color': 'hsl(359, 92%, 60%)',
                'border-top-color': 'hsl(359, 92%, 60%)',
                'border-bottom-color': '#DCDCDC',
                'border-top-left-radius': '4px',
                'border-top-right-radius': '4px',
                'border-style': 'solid',
                'border-width': '1px',
            };
        }

        buttons.push(
            <button
                className="TabsMenuItem"
                style={style}
                name={item.name}
                onClick={() => {
                    setActiveTabComponent(item.component);
                    setActiveTabsMenuItem(item.name);
                }}
            >
                {item.name}
            </button>
        );
        return true;
    });

    return (
        <div className="Tabs">
            <div className="TabsMenu">{buttons}</div>
            <div className="Tab">{activeTabComponent}</div>
        </div>
    );
}
export default Tabs;
