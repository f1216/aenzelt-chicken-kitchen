import Commands from './components/commands';
import Tabs from './components/tabs';
import './css/app.css';
import { useEffect, useState } from 'react';

function App() {
    const [componentForRerender, setComponentForRerender] = useState('no');
    useEffect(() => {
        setComponentForRerender('no');
    }, [componentForRerender]);

    return (
        <div>
            <div className="MainLogo">
                <img alt="logo" src="chicken.svg" />
            </div>
            <Commands stateChanger={setComponentForRerender} />
            <Tabs key={componentForRerender} />
        </div>
    );
}

export default App;
