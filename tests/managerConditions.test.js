const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Trash = require('../app/lib/trash');
const ManagerBuy = require('../app/managers/managerBuy');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');

Customers.customers = [
    { 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 777 },
];
Warehouse.warehouse = [
    { item: 'Chocolate', qty: 5 },
    { item: 'Asparagus', qty: 0 },
    { item: 'Milk', qty: 0 },
    { item: 'Honey', qty: 0 },
];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 5 },
    { 'Base ingredients': 'Milk', Price: 5 },
    { 'Base ingredients': 'Honey', Price: 6 },
];

test('Check conditionsChecks, get false if no ingredients in warehouse', () => {
    const customerName = 'Andrew Enzelt';
    const foodName = 'Youth Sauce';
    const customer = Customers.getCustomerByName(customerName);
    const food = Food.getFoodByName(foodName);

    const ingredients = Food.getIngredientsByFood(food, [], BaseIngredients);
    const total = ManagerBuy.calculateBuyTotal(
        ingredients,
        BaseIngredients,
        Restaurant
    );

    expect(
        ManagerConditions.conditionsChecks(
            ['test conditions'],
            customerName,
            foodName,
            customer,
            food,
            ingredients,
            total,
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            Manager,
            ManagerFile,
            Audit,
            Tax,
            'test conditions',
            false,
            Helper
        )
    ).toBe('ERROR_NO_INGREDIENTS');
});
