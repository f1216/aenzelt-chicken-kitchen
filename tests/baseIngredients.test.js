const BaseIngredients = require('../app/lib/baseIngredients');
const Warehouse = require('../app/lib/warehouse');
const Trash = require('../app/lib/trash');
const Food = require('../app/lib/food');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');

Warehouse.warehouse = [{ item: 'Chocolate', qty: 5 }];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 50 },
    { 'Base ingredients': 'Milk', Price: 750 },
    { 'Base ingredients': 'Honey', Price: 15 },
];
Trash.trash = {
    qty: 0,
    pull: [],
};

test('Check isBaseIngredient, get false if non existing name passed', () => {
    expect(BaseIngredients.isBaseIngredient('F')).toBeFalsy();
});

test('Check isSpoiled, get false if not spoiled', () => {
    jest.mock('../app/helper', () => {
        const originalModule = jest.requireActual('../app/helper');

        //Mock the default export and named export 'foo'
        return {
            __esModule: true,
            ...originalModule,
            default: jest.fn(() => 'mocked helper'),
            getRandomChance: () => {
                return 0.12123;
            },
        };
    });

    expect(
        BaseIngredients.isSpoiled(
            'Chocolate',
            1,
            Trash,
            Warehouse,
            ManagerFile,
            Audit,
            Tax,
            'Buy',
            Helper
        )
    ).toBeFalsy();
});
