const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Trash = require('../app/lib/trash');
const ManagerBuy = require('../app/managers/managerBuy');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerOrder = require('../app/managers/managerOrder');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');

Customers.customers = [
    { 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 2000 },
];
Warehouse.warehouse = [
    { item: 'Chocolate', qty: 5 },
    { item: 'Asparagus', qty: 5 },
    { item: 'Milk', qty: 5 },
    { item: 'Honey', qty: 5 },
];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 50 },
    { 'Base ingredients': 'Milk', Price: 750 },
    { 'Base ingredients': 'Honey', Price: 15 },
];

Restaurant.setBudgetBOSS(
    50000,
    Warehouse,
    ManagerFile,
    Audit,
    'test orderIngredient'
);

jest.mock('../app/helper', () => {
    const originalModule = jest.requireActual('../app/helper');

    //Mock the default export and named export 'foo'
    return {
        __esModule: true,
        ...originalModule,
        default: jest.fn(() => 'mocked helper'),
        getRandomChance: () => {
            return 0.2222;
        },
        getRandomChanceTips: () => {
            return 0.6;
        },
        getRandomChanceVolatility: () => {
            return 0.4;
        },
    };
});
test('Check orderIngredient, return correct message if all right(volatility off)', () => {
    expect(
        Restaurant.orderIngredient(
            'Milk',
            2,
            BaseIngredients,
            Food,
            Warehouse,
            Trash,
            Restaurant,
            ManagerFile,
            Audit,
            Tax,
            'test order ingredient',
            Helper
        )
    ).toEqual('Restaurant ordered Milk x2, tax: 150, spend: 1485');
});
