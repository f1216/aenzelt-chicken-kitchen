const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Trash = require('../app/lib/trash');
const ManagerBuy = require('../app/managers/managerBuy');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');
const Recommendations = require("../app/lib/recommendations");
const Statistic = require("../app/lib/statistic");

Customers.customers = [
    { 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 2000 },
];
Warehouse.warehouse = [
    { item: 'Chocolate', qty: 5 },
    { item: 'Asparagus', qty: 5 },
    { item: 'Milk', qty: 5 },
    { item: 'Honey', qty: 5 },
];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 500 },
    { 'Base ingredients': 'Milk', Price: 750 },
    { 'Base ingredients': 'Honey', Price: 15 },
];

test('Check processBuy, return false if non existing customer or food passed', () => {
    expect(
        ManagerBuy.processBuy(
            ['Andrew Enzelt', 'Youth Sauces'],
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            Manager,
            ManagerConditions,
            ManagerFile,
            Audit,
            Tax,
            Helper,
            Recommendations,
            Statistic
        )
    ).toBeFalsy();
});

test('Check processBuy, return false if allergic', () => {
    Customers.customers = [
        {
            'Regular customer': 'Andrew Enzelt',
            Allergies: 'Milk',
            Budget: 2000,
        },
    ];
    expect(
        ManagerBuy.processBuy(
            ['Andrew Enzelt', 'Youth Sauce'],
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            Manager,
            ManagerConditions,
            ManagerFile,
            Audit,
            Tax,
            Helper,
            Recommendations,
            Statistic
        )
    ).toBeFalsy();
});

test('Check processBuy, return false if low budget', () => {
    expect(
        ManagerBuy.processBuy(
            ['Andrew Enzelt', 'Youth Sauce'],
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            Manager,
            ManagerConditions,
            ManagerFile,
            Audit,
            Tax,
            Helper,
            Recommendations,
            Statistic
        )
    ).toBeFalsy();
});

test('Check processBuy, return false if there are spoiled ingredients', () => {
    Helper.getRandomChance = jest.fn(Helper.getRandomChance).mockImplementation(function() {
        return 0.1;
    });

    expect(
        ManagerBuy.processBuy(
            ['Andrew Enzelt', 'Youth Sauce'],
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            Manager,
            ManagerConditions,
            ManagerFile,
            Audit,
            Tax,
            Helper,
            Recommendations,
            Statistic
        )
    ).toBeFalsy();
});

test('Check processBuy, return true if there are no spoiled ingredients', () => {
    Customers.customers = [
        { 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 2000 },
    ];

    Helper.getRandomChance = jest.fn(Helper.getRandomChance).mockImplementation(function() {
        return 0.2222;
    });

    expect(
        ManagerBuy.processBuy(
            ['Andrew Enzelt', 'Youth Sauce'],
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            Manager,
            ManagerConditions,
            ManagerFile,
            Audit,
            Tax,
            Helper,
            Recommendations,
            Statistic
        )
    ).toBeTruthy();
});

test('Check processBuy, return correct message if all right', () => {

    Helper.getRandomChance = jest.fn(Helper.getRandomChance).mockImplementation(function() {
        return 0.2222;
    });

    Helper.getRandomChanceTips = jest.fn(Helper.getRandomChanceTips).mockImplementation(function() {
        return 0.6;
    });

    Helper.getRandomChanceVolatility = jest.fn(Helper.getRandomChanceVolatility).mockImplementation(function() {
        return 0.4;
    });

    expect(
        ManagerBuy.placeBuy(
            'Andrew Enzelt',
            'Youth Sauce',
            Customers.getCustomerByName('Andrew Enzelt'),
            Food.getFoodByName('Youth Sauce'),
            BaseIngredients.getIngredientsPrice(
                Food.getIngredientsByFood(
                    Food.getFoodByName('Youth Sauce'),
                    [],
                    BaseIngredients
                )
            ),
            Food,
            BaseIngredients,
            Warehouse,
            Trash,
            Customers,
            Restaurant,
            ManagerFile,
            Audit,
            Tax,
            'test place buy',
            Helper,
            Recommendations,
            Statistic
        )
    ).toEqual(
        'Andrew Enzelt - Youth Sauce: success, money: 1138.5, tax: 126.5, tips: 50'
    );
});
