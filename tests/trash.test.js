const BaseIngredients = require('../app/lib/baseIngredients');
const Warehouse = require('../app/lib/warehouse');
const Trash = require('../app/lib/trash');
const Food = require('../app/lib/food');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');

Warehouse.warehouse = [{ item: 'Chocolate', qty: 5 }];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 50 },
    { 'Base ingredients': 'Milk', Price: 750 },
    { 'Base ingredients': 'Honey', Price: 15 },
];
Trash.trash = {
    qty: 0,
    pull: [],
};

test('Check addTrash, get trashItem with properly increased qty on every iteration', () => {
    const ingredient = 'Chocolate';
    let qty = 1;
    for (let i = 1; i < 5; i++) {
        expect(
            Trash.addTrash(
                ingredient,
                qty,
                BaseIngredients.getIngredientsPrice([ingredient]),
                ManagerFile,
                Audit,
                Tax,
                'test'
            )
        ).toEqual({
            ingredient: ingredient,
            qty: i,
        });
    }
});
