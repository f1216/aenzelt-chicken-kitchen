const FileManager = require('../app/managers/managerFile');

test('Check getOrdersListFromInput function with empty params', () => {
    expect(FileManager.getCommandListFromInput(null)).toStrictEqual([]);
});

test('Check checkOrderData function with empty params', () => {
    expect(FileManager.checkCommandData(null)).toBeFalsy();
});

test('Check checkOrderData function with empty params', () => {
    expect(FileManager.checkCommandData(null)).toBeFalsy();
});
