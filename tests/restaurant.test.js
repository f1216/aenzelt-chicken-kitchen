const Restaurant = require('../app/restaurant');
const Warehouse = require('../app/lib/warehouse');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const conf = require('../conf');


test('Check reduceFromBudget, check if getBudget return float number', () => {
    conf.budget = 500;
    Restaurant.load();
    Restaurant.reduceFromBudget('70', Warehouse, ManagerFile, Audit, 'Budget');
    expect(Restaurant.getBudget()).toBe(430);
});

test('Check reduceFromBudget, check if return false if string passed', () => {
    Restaurant.load();
    expect(
        Restaurant.reduceFromBudget(
            'івів',
            Warehouse,
            ManagerFile,
            Audit,
            'Budget'
        )
    ).toBeFalsy();
});
