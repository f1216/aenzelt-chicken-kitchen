const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Trash = require('../app/lib/trash');
const ManagerBuy = require('../app/managers/managerBuy');
const ManagerBudget = require('../app/managers/managerBudget');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');
const Time = require('../app/lib/time');
const Recommendations = require('../app/lib/recommendations');
const Statistic = require('../app/lib/statistic');

describe('class managerBudget', () => {
	beforeEach(() => {

		Customers.customers = [
			{ 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 2000 },
			{ 'Regular customer': 'Adam Smith', Allergies: '', Budget: 2000 }
		];
		Warehouse.warehouse = [
			{ item: 'Chocolate', qty: 5 },
			{ item: 'Asparagus', qty: 5 },
			{ item: 'Milk', qty: 5 },
			{ item: 'Honey', qty: 5 },
		];
		Food.food = [
			{ Food: 'Chocolate', Ingredients: 'Chocolate' },
			{ Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
		];
		BaseIngredients.baseIngredients = [
			{ 'Base ingredients': 'Chocolate', Price: 5 },
			{ 'Base ingredients': 'Asparagus', Price: 50 },
			{ 'Base ingredients': 'Milk', Price: 750 },
			{ 'Base ingredients': 'Honey', Price: 15 },
		];
		Trash.trash = {
			qty: 0,
			pull: [],
		};


		conf.base_time_for_buy = 5;
		conf.base_time_for_table_person = 1.05;
		conf.base_time_for_order_total = 10;
		conf.base_time_for_order = 1000;
		conf.base_time_for_order_ingredient = 1.05;
		conf.base_time_for_order_dish = 1.1;
	});

	test('check processBudget return false if no data or data is undefined', () => {
		expect(ManagerBudget.processBudget(
			undefined,  Warehouse, Restaurant, ManagerFile, Audit, Tax
		)).toBeFalsy();
	});

	test('check processBudget return false if no available operations passed(not =;+;-)', () => {
		expect(ManagerBudget.processBudget(
			['biba', '500'],  Warehouse, Restaurant, ManagerFile, Audit, Tax
		)).toBeFalsy();
	});

	test('check processBudget return true if proper operations passed(like =;+;-) and proper data passed (like 500)', () => {
		expect(ManagerBudget.processBudget(
			['+', '500'],  Warehouse, Restaurant, ManagerFile, Audit, Tax
		)).toBeTruthy();
	});
});