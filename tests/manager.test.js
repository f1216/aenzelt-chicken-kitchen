const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Trash = require('../app/lib/trash');
const ManagerBuy = require('../app/managers/managerBuy');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');
const Time = require('../app/lib/time');
const Recommendations = require('../app/lib/recommendations');
const Statistic = require('../app/lib/statistic');

test('Check prepareCommandList function with empty params', () => {
    expect(Manager.prepareCommandList('', '', '', ManagerFile,  Time, Food, BaseIngredients, Helper, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse)).toStrictEqual([
        { command: '', data: [], time: 0},
    ]);
});

test('Check prepareOrdersList function with undefined params', () => {
    expect(
        Manager.prepareCommandList(null, null, null, ManagerFile,  Time, Food, BaseIngredients, Helper, Recommendations, Customers, Restaurant, Audit, Statistic, Warehouse)
    ).toStrictEqual([{ command: '', data: [], time: 0 }]);
});
