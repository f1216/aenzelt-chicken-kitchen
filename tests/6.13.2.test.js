const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Trash = require('../app/lib/trash');
const ManagerBuy = require('../app/managers/managerBuy');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');
const Time = require('../app/lib/time');
const Recommendations = require('../app/lib/recommendations');
const Statistic = require('../app/lib/statistic');

describe('6.13.2 - tests for demonstrating properly working functionality', () => {
	beforeEach(() => {

		Audit.audit.Resource = [];

		Customers.customers = [
			{ 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 10000 },
			{ 'Regular customer': 'Adam Smith', Allergies: '', Budget: 10000 }
		];

		Food.food = [
			{ Food: 'Emperor Chicken', Ingredients: 'Fat Cat Chicken, Spicy Sauce, Tuna Cake' },
			{ Food: 'Fat Cat Chicken', Ingredients: 'Princess Chicken, Youth Sauce, Fries, Diamond Salad' },
			{ Food: 'Princess Chicken', Ingredients: 'Chicken, Youth Sauce, Youth Sauce' },
			{ Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
			{ Food: 'Spicy Sauce', Ingredients: 'Paprika, Garlic, Water' },
			{ Food: 'Omega Sauce', Ingredients: 'Lemon, Water' },
			{ Food: 'Diamond Salad', Ingredients: 'Tomatoes, Pickles, Feta' },
			{ Food: 'Ruby Salad', Ingredients: 'Tomatoes, Vinegar, Chocolate' },
			{ Food: 'Fries', Ingredients: 'Potatoes' },
			{ Food: 'Smashed Potatoes', Ingredients: 'Potatoes' },
			{ Food: 'Tuna Cake', Ingredients: 'Tuna, Chocolate, Youth Sauce' },
			{ Food: 'Fish In Water', Ingredients: 'Tuna, Omega Sauce, Ruby Salad' },
			{ Food: 'Irish Fish', Ingredients: 'Tuna, Fries, Smashed Potatoes' },
		];
		BaseIngredients.baseIngredients = [
			{ 'Base ingredients': 'Chicken', Price: 10 },
			{ 'Base ingredients': 'Tuna', Price: 50 },
			{ 'Base ingredients': 'Potatoes', Price: 750 },
			{ 'Base ingredients': 'Asparagus', Price: 15 },
			{ 'Base ingredients': 'Milk', Price: 5 },
			{ 'Base ingredients': 'Honey', Price: 50 },
			{ 'Base ingredients': 'Paprika', Price: 750 },
			{ 'Base ingredients': 'Garlic', Price: 15 },
			{ 'Base ingredients': 'Water', Price: 5 },
			{ 'Base ingredients': 'Lemon', Price: 50 },
			{ 'Base ingredients': 'Tomatoes', Price: 750 },
			{ 'Base ingredients': 'Pickles', Price: 15 },
			{ 'Base ingredients': 'Feta', Price: 5 },
			{ 'Base ingredients': 'Vinegar', Price: 50 },
			{ 'Base ingredients': 'Rice', Price: 750 },
			{ 'Base ingredients': 'Chocolate', Price: 15 },

		];
		Trash.trash = {
			qty: 0,
			pull: [],
		};
	});

	test('Check processBuy(Emperor Chicken), need to return true(confirmation that we can make Emperor Chicken)', () => {

		Helper.getRandomChance = jest.fn(Helper.getRandomChance).mockImplementation(function() {
			return 0.6;
		});

		Warehouse.warehouse = [
			{ item: 'Chicken', qty: 50 },
			{ item: 'Tuna', qty: 50 },
			{ item: 'Potatoes', qty: 750 },
			{ item: 'Asparagus', qty: 50 },
			{ item: 'Milk', qty: 50 },
			{ item: 'Honey', qty: 50 },
			{ item: 'Paprika', qty: 750 },
			{ item: 'Garlic', qty: 50 },
			{ item: 'Water', qty: 50 },
			{ item: 'Lemon', qty: 50 },
			{ item: 'Tomatoes', qty: 750 },
			{ item: 'Pickles', qty: 50 },
			{ item: 'Feta', qty: 50 },
			{ item: 'Vinegar', qty: 50 },
			{ item: 'Rice', qty: 750 },
			{ item: 'Chocolate', qty: 50 },
		];

		//aeiou 11
		expect(
			ManagerBuy.processBuy(
				['Andrew Enzelt', 'Emperor Chicken'],
				Food,
				BaseIngredients,
				Warehouse,
				Trash,
				Customers,
				Restaurant,
				Manager,
				ManagerConditions,
				ManagerFile,
				Audit,
				Tax,
				Helper,
				Recommendations,
				Statistic
			)
		).toBeTruthy();
	});

	test('Check processBuy(Emperor Chicken) if we have spoiled ingredient, need to return false(confirmation that we can not make Emperor Chicken)', () => {

		Helper.getRandomChance = jest.fn(Helper.getRandomChance).mockImplementation(function() {
			return 0.01;
		});

		Warehouse.warehouse = [
			{ item: 'Chicken', qty: 1 },
			{ item: 'Tuna', qty: 50 },
			{ item: 'Potatoes', qty: 750 },
			{ item: 'Asparagus', qty: 15 },
			{ item: 'Milk', qty: 50 },
			{ item: 'Honey', qty: 50 },
			{ item: 'Paprika', qty: 750 },
			{ item: 'Garlic', qty: 15 },
			{ item: 'Water', qty: 50 },
			{ item: 'Lemon', qty: 50 },
			{ item: 'Tomatoes', qty: 750 },
			{ item: 'Pickles', qty: 15 },
			{ item: 'Feta', qty: 50 },
			{ item: 'Vinegar', qty: 50 },
			{ item: 'Rice', qty: 750 },
			{ item: 'Chocolate', qty: 15 },
		];

		//aeiou 11
		expect(
			ManagerBuy.processBuy(
				['Andrew Enzelt', 'Emperor Chicken'],
				Food,
				BaseIngredients,
				Warehouse,
				Trash,
				Customers,
				Restaurant,
				Manager,
				ManagerConditions,
				ManagerFile,
				Audit,
				Tax,
				Helper,
				Recommendations,
				Statistic
			)
		).toBeFalsy();
	});

	test('Check processInput(Main command) Audit should have only one command ', () => {

		ManagerFile.getCommandListFromInput = jest.fn(ManagerFile.getCommandListFromInput).mockImplementation(function() {
		return [
			{command: 'Buy', data: ['Andrew Enzelt', 'Chocolate']},
			{command: 'Order', data: ['Potatoes', 10]},
			{command: 'Audit', data: ['Resources']},]
		});
		Manager.processInput('', '', '');
		//aeiou 11
		expect(
			Audit.getAuditLength()
		).toBe(1);
	});
});
