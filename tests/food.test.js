const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
const Warehouse = require('../app/lib/warehouse');
Warehouse.warehouse = [{ item: 'Chocolate', qty: 5 }];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 50 },
    { 'Base ingredients': 'Milk', Price: 5 },
    { 'Base ingredients': 'Honey', Price: 15 },
];

test('Check getIngredientsByFood, return [] if non existing foodname item passed', () => {
    expect(
        Food.getIngredientsByFood('Youth Saudce', [], BaseIngredients)
    ).toStrictEqual([]);
});
