const Warehouse = require('../app/lib/warehouse');
const Customers = require('../app/lib/customers');
const conf = require('../conf');
const Food = require('../app/lib/food');
const Trash = require('../app/lib/trash');
const BaseIngredients = require('../app/lib/baseIngredients');
const ManagerBuy = require('../app/managers/managerBuy');
const ManagerTable = require('../app/managers/managerTable');
const Restaurant = require('../app/restaurant');
const Manager = require('../app/managers/manager');
const ManagerConditions = require('../app/managers/managerConditions');
const ManagerFile = require('../app/managers/managerFile');
const Audit = require('../app/lib/audit');
const Tax = require('../app/tax');
const Helper = require('../app/helper');
const Recommendations = require("../app/lib/recommendations");
const Statistic = require("../app/lib/statistic");
const ManagerTrash = require("../app/managers/managerTrash");


describe('class managerTable', () => {
    beforeEach(() => {

        BaseIngredients.checkSpoiled = jest.fn(BaseIngredients.checkSpoiled).mockImplementation(function() {
            return false;
        });

        Customers.customers = [
            { 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 777 },
        ];
        Warehouse.warehouse = [{ item: 'Chocolate', qty: 5 }];
        Food.food = [
            { Food: 'Chocolate', Ingredients: 'Chocolate' },
            { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
        ];
        BaseIngredients.baseIngredients = [
            { 'Base ingredients': 'Chocolate', Price: 5 },
            { 'Base ingredients': 'Asparagus', Price: 50 },
            { 'Base ingredients': 'Milk', Price: 750 },
            { 'Base ingredients': 'Honey', Price: 15 },
        ];


        conf.base_time_for_buy = 5;
        conf.base_time_for_table_person = 1.05;
        conf.base_time_for_order_total = 10;
        conf.base_time_for_order = 1000;
        conf.base_time_for_order_ingredient = 1.05;
        conf.base_time_for_order_dish = 1.1;
    });


    test('Check processTable, return false if non existing customer or food passed', () => {
        expect(
            ManagerTable.processTable(
                ['Andrew Enzelt', 'Youth Sauces'],
                Food,
                BaseIngredients,
                Warehouse,
                Trash,
                Customers,
                Restaurant,
                Manager,
                ManagerConditions,
                ManagerFile,
                ManagerBuy,
                Audit,
                Tax,
                Helper,
                Recommendations,
                Statistic
            )
        ).toBeFalsy();
    });

    test('Check processTable, return false if allergic', () => {
        expect(
            ManagerTable.processTable(
                ['Andrew Enzelt', 'Youth Sauce'],
                Food,
                BaseIngredients,
                Warehouse,
                Trash,
                Customers,
                Restaurant,
                Manager,
                ManagerConditions,
                ManagerFile,
                ManagerBuy,
                Audit,
                Tax,
                Helper,
                Recommendations,
                Statistic
            )
        ).toBeFalsy();
    });

    test('Check processTable, return false if low budget', () => {
        expect(
            ManagerTable.processTable(
                ['Andrew Enzelt', 'Youth Sauce'],
                Food,
                BaseIngredients,
                Warehouse,
                Trash,
                Customers,
                Restaurant,
                Manager,
                ManagerConditions,
                ManagerFile,
                ManagerBuy,
                Audit,
                Tax,
                Helper,
                Recommendations,
                Statistic
            )
        ).toBeFalsy();
    });

    test('Check processTable, return true if everything okay', () => {

        Customers.customers = [
            { 'Regular customer': 'Andrew Enzelt', Allergies: '', Budget: 777 },
        ];

        expect(
            ManagerTable.processTable(
                ['Andrew Enzelt', 'Chocolate'],
                Food,
                BaseIngredients,
                Warehouse,
                Trash,
                Customers,
                Restaurant,
                Manager,
                ManagerConditions,
                ManagerFile,
                ManagerBuy,
                Audit,
                Tax,
                Helper,
                Recommendations,
                Statistic
            )
        ).toBeTruthy();
    });
});





