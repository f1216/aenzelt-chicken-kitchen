const chai = require("chai");
const spies = require("chai-spies");
chai.use(spies);
const Customers = require('../app/lib/customers');
const BaseIngredients = require('../app/lib/baseIngredients');
const Food = require('../app/lib/food');
const Helper = require('../app/helper');
Customers.customers = [{ 'Regular customer': 'Andrew Enzelt', Budget: 100 }];

test('Check getCustomerByName, return false if non existing name passed', () => {
    expect(Customers.getCustomerByName('Andresw Enzelt')).toBeFalsy();
});

test('Check getBudget, return false if non existing customer passed or invalid object passed', () => {
    expect(
        Customers.getBudget({ 'Ansdrew Enzelt': { budget: 0 } })
    ).toBeFalsy();
});
//No needed, but still exist
test('Check reduceFromBudget, return false if non existing customer passed or invalid total passed', () => {
    expect(
        Customers.reduceFromBudget(
            Customers.getCustomerByName('Andrew Enzelt'),
            'wq'
        )
    ).toBeFalsy();
});

test('Check checkAllergen, return false if no allergen in food', () => {
    expect(
        Customers.checkAllergen(
            Customers.getCustomerByName('Andrew Enzelt'),
            ['Chocolate'],
            BaseIngredients
        )
    ).toBeFalsy();
});


test('Check checkWants, return wantCounter - amount of wants ingredients', () => {
    let ingredients = []
    let wantCounter = 0
    let wantIngridientsList = []
    BaseIngredients.baseIngredients = ["Chicken", "Tuna", "Potatoes", "Asparagus", "Milk", "Honey", "Paprika"];
    Food.getIngredientsByFood = jest.fn(Food.getIngredientsByFood).mockImplementationOnce(function() {
        return ["Chicken", "Tuna"];
    })
    Helper.getRandomChanceWants = jest.fn(Helper.getRandomChanceWants).mockImplementationOnce(function() {
        return 88;
    })
    const randomWant =  Customers.checkWants(Helper, BaseIngredients, Food, 'Spicy Sauce')
    expect(randomWant).toBeLessThanOrEqual(3)



    // let ingredients = []
    // let wantCounter = 0
    // let wantIngridientsList = []
    // BaseIngredients.baseIngredients = ["Chicken", "Tuna", "Potatoes", "Asparagus", "Milk", "Honey", "Paprika"];
    // // chai.spy.on(Food, "getIngredientsByFood", () => { return ["Chicken", "Tuna"] });
    // // chai.spy.on(Helper, "getRandomChanceWants", () => 88);
    // const randomWant =  Customers.checkWants(Helper, BaseIngredients, Food, 'Spicy Sauce')
    // expect(randomWant).toBeLessThanOrEqual(3)

})
