const Warehouse = require('../app/lib/warehouse');
const conf = require('../conf');
const Food = require('../app/lib/food');
const BaseIngredients = require('../app/lib/baseIngredients');
Warehouse.warehouse = [{ item: 'Chocolate', qty: 5 }];
Food.food = [
    { Food: 'Chocolate', Ingredients: 'Chocolate' },
    { Food: 'Youth Sauce', Ingredients: 'Asparagus, Milk, Honey' },
];
BaseIngredients.baseIngredients = [
    { 'Base ingredients': 'Chocolate', Price: 5 },
    { 'Base ingredients': 'Asparagus', Price: 50 },
    { 'Base ingredients': 'Milk', Price: 5 },
    { 'Base ingredients': 'Honey', Price: 15 },
];

test('Check getQty, return conf  default_item_qty  if non existing BASE item passed', () => {
    expect(Warehouse.getQty(Warehouse.getItemByName('Chocolateas'), true)).toBe(
        conf.default_item_qty
    );
});

test('Check checkAvailability, return false if low qty', () => {
    expect(
        Warehouse.checkAvailability('Chocolate', 6, Food, BaseIngredients)
    ).toEqual(false);
});

test('Check checkAvailability, return false if not exists qty', () => {
    expect(
        Warehouse.checkAvailability('Chocolatse', 6, Food, BaseIngredients)
    ).toEqual(false);
});
