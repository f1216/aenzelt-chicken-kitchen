const Loader = require('./app/loader');
const Uploader = require('./app/uploader');
const Manager = require('./app/managers/manager');
const yargs = require('yargs');
//
// const options = yargs
//     .usage("Usage: -i <input> -c <command> -d <data>")
//     .option("i", { alias: "file", describe: "Use file input", type: "string", demandOption: false })
//     .option("c", { alias: "command", describe: "Command", type: "string", demandOption: false })
//     .option("d", { alias: "data", describe: "Data, please separate buy coma ', '", type: "string", demandOption: false })
//     .argv;
//
// if (options.i === undefined &&
//     (options.c === undefined || options.d === undefined) ) {
//     console.log('There are no input(both, files or params), please try again!');
//     process.exit();
// }
//
// const command = options.command ?? '';
// const data = options.data ? options.data.split(', ') : '';
// const fileName = options.file ?? '';

/** Here we have enter point of our application,
 * we getting one command(like Buy,Order etc.) from console(getting console arguments),
 * and here we can enter input fileName with commands,
 * (input file should be in input directory of application)
 * (then we can try add functionality fot getting input file from any path)*/

/** Here we call load function from loader,
 * on this stage all data(like customers, food, warehouse etc.) loading to classes(application memory)
 * all base data stored in data directory*/
/** Тут ми викликаємо функцію навантаження з завантажувача,
 * на цьому етапі всі дані (наприклад, клієнти, продукти харчування, склад тощо) завантажуються в класи (пам'ять програми)
 * всі базові дані зберігаються в каталозі даних*/
Loader.load();
// Manager.processInput(command, data, fileName);
/** Here we start to process all commands that we have*/
Manager.processInput('', '', 'input');
/** Here we have finish stage, we uploading updated data to files*/
Uploader.upload();

module.exports = Manager

